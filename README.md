# The Weather App

## General Info

### Project description
The Weather app is an iPhone app that allows users to check the weather forecast for their current location, as well as checking the forecast for other locations. Users are also able to see both hourly forecasts for the next 24 hours and predictions for the next 5 days.

### Technology stack 
* Language - Swift 4.2
* Deployment version - iOS 11.0+
* IDE - Xcode 
* Tools - CocoaPods
* Open Source Libraries - SwiftLint, Alamofire, CodableAlamofire, SwiftyBeaver, Quick and Nimble
* API - [World Weather Online](https://www.worldweatheronline.com/developer/api)

## How to setup
* Checkout the latest version of the code with the *git clone* command
* Ensure that you have CocoaPods installed on your machine (See the installation manual [here](https://cocoapods.org/))
* From 09 March 2019, you'll need to create a free account on [World Weather Online](https://www.worldweatheronline.com/developer/api) in order to get a new API-key. Once you have the API-key, follow these steps:
	* Open *TheWeatherApp.xcworkspace*
	* Go to *TheWeatherApp* target
	* Open the tab *Build Settings*
	* Search for *API_CLIENT_KEY*
	* Replace the old key for the new API-key
* Optional: Install your personal certificates to run the application on a physical device. Otherwise use the simulators.


## Design Phase
### Requirements specification
It's important to first ascertain which are the client's requirements and therefore which features should be included in the MVP. In identifying these requirements, we are then able to estimate how much time it will take to implement each one, which will inform our decision on their inclusion. 

### Wireframes
After settling on the requirements for the app, the next step is to decide on its appearance. In the wireframe, we're able to definite the screens of the app and their content, the user interaction, and the navigation flow. 

### Mockups
Using the wireframes, we are able to accurately draft the layout of each screen. From here, we are able to transform them into the final mock-up for the app.


## Development Phase

### Architecture
The app was built using Clean Swift architecture. For more information, see the [Clean Swift website](https://clean-swift.com)

### Testing strategy
Clean Architecture splits up the application into Scenes, each with its own VIP-Cycle. This cycle contains components, each with one single responsibility. These components define their inputs/outputs using protocols, making the testing of the app easier and more straightforward.

All the VIP-cycle tests are found in the WeatherTests folder.

## Contact
* Martin Pastorin - [Linkedin](https://linkedin.com/in/mpastorin)
