//
//  XCTestCase+Extension.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 20/01/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import XCTest

extension XCTestCase {
    
    func load(view: UIView, in window: UIWindow) {
        window.addSubview(view)
        RunLoop.current.run(until: Date())
    }
}
