//
//  LocationTitleViewModel+Extension.swift
//  TheWeatherAppTests
//
//  Created by Martin Pastorin on 20/01/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

@testable import TheWeatherApp

extension LocationTitleViewModel {
    func isEqualTo(_ other: LocationTitleViewModel) -> Bool {
        return locationName == other.locationName &&
            date == other.date &&
            iconName == other.iconName
    }
}
