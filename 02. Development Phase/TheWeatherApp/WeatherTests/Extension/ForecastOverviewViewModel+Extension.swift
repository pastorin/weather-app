//
//  ForecastOverviewViewModel+Extension.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 20/01/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

@testable import TheWeatherApp

extension ForecastOverviewViewModel {
    func isEqualTo(_ other: ForecastOverviewViewModel) -> Bool {
        return locationName == other.locationName &&
            date == other.date &&
            iconName == other.iconName &&
            weatherDescription == other.weatherDescription &&
            currentTemperature == other.currentTemperature &&
            feelsLike == other.feelsLike &&
            minTemperature == other.minTemperature &&
            humidity == other.humidity &&
            precipitation == other.precipitation &&
            visibility == other.visibility &&
            wind == other.wind
    }
}
