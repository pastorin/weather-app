//
//  Seeds.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 20/01/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

@testable import TheWeatherApp
import Foundation

enum Seeds {
    
    enum ForecastTypes {
        static let `default` = ForecastType.locationName(name: LocationNames.default)
    }
    
    enum Forecasts {
        static let `default` = Forecast(type: .city,
                                        locationName: ForecastTypes.default.locationName,
                                        date: Dates.default,
                                        weatherDescription: WeatherDescription.default,
                                        iconCode: IconCode.default,
                                        humidity: Humidity.default,
                                        precipitation: Precipitation.default,
                                        visibility: Visibility.default,
                                        wind: Winds.default,
                                        celsiusTemperature: Temperatures.celciusDefault,
                                        fahrenheitTemperature: Temperatures.fahrenheitDefault,
                                        hourlyForecast: [],
                                        dailyForecast: [])
    }
    
    enum LocationNames {
        static let `default` = "Amsterdam"
    }
    
    enum Winds {
        static let `default` = Wind(direction: "ENE", degree: "70", speed: 7)
        static let defaultRedable = "\(Winds.default.direction) \(Winds.default.speed.kilometrePerHourReadable())"
    }
    
    enum Temperatures {
        static let celciusDefault = Temperature(current: 18, feelsLike: 18, min: 17, max: 20)
        static let fahrenheitDefault = Temperature(current: 64, feelsLike: 64, min: 62, max: 68)
        
        static let celciusDefaultRedable = ( current: celciusDefault.current?.temperatureReadable(), feelsLike: celciusDefault.feelsLike?.temperatureReadable(), min: celciusDefault.min?.temperatureReadable(), max: celciusDefault.max?.temperatureReadable())
        static let fahrenheitDefaultRedable = ( current: fahrenheitDefault.current?.temperatureReadable(), feelsLike: fahrenheitDefault.feelsLike?.temperatureReadable(), min: fahrenheitDefault.min?.temperatureReadable(), max: fahrenheitDefault.max?.temperatureReadable())
    }
    
    enum WeatherDescription {
        static let `default` = "Sunny"
        static let defaultRedable = WeatherDescription.default
    }
    
    enum IconCode {
        static let `default` = "10"
        static let defaultRedable = IconCode.default.iconFilename()
    }
    
    enum Dates {
        static let `default` = Date()
        static let defaultRedable = Dates.default.readable()
    }
    
    enum Visibility {
        static let `default` = 10
        static let defaultRedable = Visibility.default.kilometreReadable()
    }
    
    enum Precipitation {
        static let `default`: Float = 0.3
        static let defaultRedable = Precipitation.default.precipitationReadable()
    }
    
    enum Humidity {
        static let `default` = 59
        static let defaultRedable = Humidity.default.percentageReadable()
    }
    
    enum Error {
        static let serverNotFound = NSError(domain: Bundle.main.bundleIdentifier!, code: 404, userInfo: ["error": "404 - Server Not Found"])
    }
    
    enum ForecastOverviewViewModels {
        private struct Item: ForecastOverviewViewModel {
            var locationName: String
            var date: String?
            var iconName: String?
            var weatherDescription: String?
            var currentTemperature: String?
            var feelsLike: String?
            var minTemperature: String?
            var maxTemperature: String?
            var humidity: String?
            var precipitation: String?
            var visibility: String?
            var wind: String?
        }
        
        static let `default`: ForecastOverviewViewModel = Item(locationName: LocationNames.default,
                                                               date: Dates.defaultRedable,
                                                               iconName: IconCode.defaultRedable,
                                                               weatherDescription: WeatherDescription.defaultRedable,
                                                               currentTemperature: Temperatures.celciusDefaultRedable.current,
                                                               feelsLike: Temperatures.celciusDefaultRedable.feelsLike,
                                                               minTemperature: Temperatures.celciusDefaultRedable.min,
                                                               maxTemperature: Temperatures.celciusDefaultRedable.max,
                                                               humidity: Humidity.defaultRedable,
                                                               precipitation: Precipitation.defaultRedable,
                                                               visibility: Visibility.defaultRedable,
                                                               wind: Winds.defaultRedable)
    }
    
    enum LocationTitleViewModels {
        private struct Item: LocationTitleViewModel {
            var locationName: String
            var date: String?
            var iconName: String?
        }
        
        static let `default`: LocationTitleViewModel = Item(locationName: LocationNames.default,
                                                            date: Dates.defaultRedable,
                                                            iconName: IconCode.defaultRedable)
    }
    
    enum ForecastConditionViewModels {
        private struct Item: ForecastConditionViewModel {
            var title: String
            var value: String?
        }
        static let `default`: ForecastConditionViewModel = Item(title: "Title",
                                                                value: "Value")
    }

}
