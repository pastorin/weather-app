//
//  LocationTitleViewTests.swift
//  TheWeatherAppTests
//
//  Created by Martin Pastorin on 20/01/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

@testable import TheWeatherApp
import Quick
import Nimble

class LocationTitleViewTests: QuickSpec {
    
    // MARK: Subject under test
    
    var sut: LocationTitleView!
    var window: UIWindow!
    
    override func spec() {
        describe("The ForecastOverview View") {
            beforeEach {
                self.window = UIWindow()
                let viewController = UIViewController.from(storyboard: .cityForecast) as! CityForecastViewController
                self.load(view: viewController.view, in: self.window)
                self.sut = viewController.forecastOverviewView.locationTitleView
            }
            
            afterEach {
                self.window = nil
            }
            
            describe("without data") {
                it("should display empty state") {
                    // Given
                    
                    // When
                    self.sut.data = nil
                    
                    // Then
                    expect(self.sut.locationNameLabel.text).to(equal(LocalizedKeys.Core.unknown))
                    expect(self.sut.dateLabel.text).to(equal(""))
                }
            }
            
            describe("on setting data") {
                it("should display information") {
                    // Given
                    let data = Seeds.LocationTitleViewModels.default
                        
                    // When
                    self.sut.data = data
                    
                    // Then
                    expect(self.sut.locationNameLabel.text).to(equal(data.locationName.uppercased()))
                    expect(self.sut.dateLabel.text).to(equal(data.date?.uppercased()))
                }
            }
        }
    }
}
