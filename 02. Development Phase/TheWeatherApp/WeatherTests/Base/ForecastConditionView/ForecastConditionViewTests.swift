//
//  ForecastConditionViewTests.swift
//  TheWeatherAppTests
//
//  Created by Martin Pastorin on 20/01/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

@testable import TheWeatherApp
import Quick
import Nimble

class ForecastConditionViewTests: QuickSpec {
    
    // MARK: Subject under test
    
    var sut: ForecastConditionView!
    var window: UIWindow!
    
    override func spec() {
        describe("The ForecastOverview View") {
            beforeEach {
                self.window = UIWindow()
                let viewController = UIViewController.from(storyboard: .cityForecast) as! CityForecastViewController
                self.load(view: viewController.view, in: self.window)
                self.sut = viewController.forecastOverviewView.feelsLikeConditionView
            }
            
            afterEach {
                self.window = nil
            }
            
            describe("without data") {
                it("should display empty state") {
                    // Given
                    
                    // When
                    self.sut.data = nil
                    
                    // Then
                    expect(self.sut.titleLabel.text).to(equal(""))
                    expect(self.sut.valueLabel.text).to(equal(LocalizedKeys.Core.emptyValue))
                }
            }
            
            describe("on setting data") {
                it("should display information") {
                    // Given
                    let data = Seeds.ForecastConditionViewModels.default
                    
                    // When
                    self.sut.data = data
                    
                    // Then
                    expect(self.sut.titleLabel.text).to(equal(data.title.capitalized))
                    expect(self.sut.valueLabel.text).to(equal(data.value))
                }
            }
        }
    }
}
