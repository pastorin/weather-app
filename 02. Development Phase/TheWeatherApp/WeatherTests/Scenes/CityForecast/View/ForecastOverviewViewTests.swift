//
//  ForecastOverviewViewTests.swift
//  TheWeatherAppTests
//
//  Created by Martin Pastorin on 20/01/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

@testable import TheWeatherApp
import Quick
import Nimble

class ForecastOverviewViewTests: QuickSpec {
    
    // MARK: Subject under test
    
    var sut: ForecastOverviewView!
    var window: UIWindow!
    
    override func spec() {
        describe("The ForecastOverview View") {
            beforeEach {
                self.window = UIWindow()
                let viewController = UIViewController.from(storyboard: .cityForecast) as! CityForecastViewController
                self.load(view: viewController.view, in: self.window)
                self.sut = viewController.forecastOverviewView
            }
        
            afterEach {
                self.window = nil
            }
            
            describe("without data") {
                it("should display empty state") {
                    // Given
                    
                    // When
                    self.sut.data = nil
                    
                    // Then
                    expect(self.sut.locationTitleView.data).to(beNil())
                    expect(self.sut.descriptionLabel.text).to(equal(LocalizedKeys.Core.unknown))
                    expect(self.sut.currentTempLabel.text).to(equal(LocalizedKeys.Core.emptyValue))
                    
                    expect(self.sut.feelsLikeConditionView.data?.title).to(equal(LocalizedKeys.ForecastOverview.feelsLike))
                    expect(self.sut.feelsLikeConditionView.data?.value).to(beNil())
                    
                    expect(self.sut.minConditionView.data?.title).to(equal(LocalizedKeys.ForecastOverview.min))
                    expect(self.sut.minConditionView.data?.value).to(beNil())
                    
                    expect(self.sut.maxConditionView.data?.title).to(equal(LocalizedKeys.ForecastOverview.max))
                    expect(self.sut.maxConditionView.data?.value).to(beNil())
                    
                    expect(self.sut.humidityConditionView.data?.title).to(equal(LocalizedKeys.ForecastOverview.humidity))
                    expect(self.sut.humidityConditionView.data?.value).to(beNil())
                    
                    expect(self.sut.precipitationConditionView.data?.title).to(equal(LocalizedKeys.ForecastOverview.precipitation))
                    expect(self.sut.precipitationConditionView.data?.value).to(beNil())
                    
                    expect(self.sut.visibilityConditionView.data?.title).to(equal(LocalizedKeys.ForecastOverview.visibility))
                    expect(self.sut.visibilityConditionView.data?.value).to(beNil())
                    
                    expect(self.sut.windConditionView.data?.title).to(equal(LocalizedKeys.ForecastOverview.wind))
                    expect(self.sut.windConditionView.data?.value).to(beNil())
                }
            }
            
            describe("on setting data") {
                it("should display information") {
                    // Given
                    let data = Seeds.ForecastOverviewViewModels.default
                    
                    // When
                    self.sut.data = data
                    
                    // Then
                    expect(self.sut.locationTitleView.data?.isEqualTo(data)).to(beTrue())
                    expect(self.sut.descriptionLabel.text).to(equal(data.weatherDescription))
                    expect(self.sut.currentTempLabel.text).to(equal(data.currentTemperature))
                    
                    expect(self.sut.feelsLikeConditionView.data?.title).to(equal(LocalizedKeys.ForecastOverview.feelsLike))
                    expect(self.sut.feelsLikeConditionView.data?.value).to(equal(data.feelsLike))
                    
                    expect(self.sut.minConditionView.data?.title).to(equal(LocalizedKeys.ForecastOverview.min))
                    expect(self.sut.minConditionView.data?.value).to(equal(data.minTemperature))
                    
                    expect(self.sut.maxConditionView.data?.title).to(equal(LocalizedKeys.ForecastOverview.max))
                    expect(self.sut.maxConditionView.data?.value).to(equal(data.maxTemperature))
                    
                    expect(self.sut.humidityConditionView.data?.title).to(equal(LocalizedKeys.ForecastOverview.humidity))
                    expect(self.sut.humidityConditionView.data?.value).to(equal(data.humidity))
                    
                    expect(self.sut.precipitationConditionView.data?.title).to(equal(LocalizedKeys.ForecastOverview.precipitation))
                    expect(self.sut.precipitationConditionView.data?.value).to(equal(data.precipitation))
                    
                    expect(self.sut.visibilityConditionView.data?.title).to(equal(LocalizedKeys.ForecastOverview.visibility))
                    expect(self.sut.visibilityConditionView.data?.value).to(equal(data.visibility))
                    
                    expect(self.sut.windConditionView.data?.title).to(equal(LocalizedKeys.ForecastOverview.wind))
                    expect(self.sut.windConditionView.data?.value).to(equal(data.wind))
                    
                }
            }
        }
    }
}
