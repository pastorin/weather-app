//
//  CityForecastViewControllerTests.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 20/01/2019.
//  Copyright (c) 2019 Martin Pastorin. All rights reserved.
//

@testable import TheWeatherApp
import Quick
import Nimble

class CityForecastViewControllerTests: QuickSpec {

    // MARK: Subject under test
    
    var sut: CityForecastViewController!
    var window: UIWindow!

    override func spec() {
        describe("The CityForecast ViewController") {
            beforeEach {
                self.window = UIWindow()
                self.sut = UIViewController.from(storyboard: .cityForecast) as? CityForecastViewController
            }

            afterEach {
                self.window = nil
            }

            describe("on view load") {
                it("should call call interactor to fetch forecast type") {
                    // Given
                    let spy = CityForecastInteractorInterfaceSpy()
                    self.sut.interactor = spy

                    // When
                    self.load(view: self.sut.view, in: self.window)

                    // Then
                    expect(spy.fetchForecastTypeWasCalled).to(beTrue())
                }
                
                it("should call call interactor to fetch forecast") {
                    // Given
                    let spy = CityForecastInteractorInterfaceSpy()
                    self.sut.interactor = spy
                    
                    // When
                    self.load(view: self.sut.view, in: self.window)
                    
                    // Then
                    expect(spy.fetchForecastWasCalled).to(beTrue())
                }
            }

            describe("on display forecast type") {
                context("with a valid viewModel") {
                    // Given
                    let forecastOverview = Seeds.ForecastOverviewViewModels.default
                    let viewModel = CityForecastModels.FetchForecastType.ViewModel(forecastOverview: forecastOverview)
                    
                    it("should display location information") {
                        // Given
                        self.load(view: self.sut.view, in: self.window)
                        
                        // When
                        self.sut.displayForecastType(viewModel: viewModel)
                        
                        // Then
                        let forecastOverviewView = self.sut.forecastOverviewView
                        expect(forecastOverviewView!.data!.isEqualTo(forecastOverview)).to(beTrue())
                    }
                }
            }
            
            describe("on display forecast") {
                context("with a valid viewModel") {
                    // Given
                    let forecastOverview = Seeds.ForecastOverviewViewModels.default
                    let forecast = Seeds.Forecasts.default
                    let viewModel = CityForecastModels.FetchForecast.ViewModel(forecast: forecast, forecastOverview: forecastOverview, error: nil)
                    
                    it("should display forecast") {
                        // Given
                        self.load(view: self.sut.view, in: self.window)
                        
                        // When
                        self.sut.displayForecast(viewModel: viewModel)
                        
                        // Then
                        let forecastOverviewView = self.sut.forecastOverviewView
                        let hourlyForecastDataStore = self.sut.hourlyForecastViewController?.router?.dataStore
                        let dailyForecastDataStore = self.sut.dailyForecastViewController?.router?.dataStore
                        
                        expect(forecastOverviewView!.data!.isEqualTo(forecastOverview)).to(beTrue())
                        expect(hourlyForecastDataStore!.forecast?.locationName).to(equal(forecast.locationName))
                        expect(dailyForecastDataStore!.forecast?.locationName).to(equal(forecast.locationName))
                    }
                }
                
                context("without a forecast in the viewModel") {
                    // Given
                    let forecastOverview = Seeds.ForecastOverviewViewModels.default
                    let viewModel = CityForecastModels.FetchForecast.ViewModel(forecast: nil, forecastOverview: forecastOverview, error: nil)
                    
                    it("should not display forecast") {
                        // Given
                        self.load(view: self.sut.view, in: self.window)
                        
                        // When
                        self.sut.displayForecast(viewModel: viewModel)
                        
                        // Then
                        let forecastOverviewView = self.sut.forecastOverviewView
                        let hourlyForecastDataStore = self.sut.hourlyForecastViewController?.router?.dataStore
                        let dailyForecastDataStore = self.sut.dailyForecastViewController?.router?.dataStore
                        
                        expect(forecastOverviewView!.data?.currentTemperature).to(beNil())
                        expect(hourlyForecastDataStore!.forecast).to(beNil())
                        expect(dailyForecastDataStore!.forecast).to(beNil())
                    }
                }
                
                context("without a forecast overview in the viewModel") {
                    // Given
                    let forecast = Seeds.Forecasts.default
                    let viewModel = CityForecastModels.FetchForecast.ViewModel(forecast: forecast, forecastOverview: nil, error: nil)
                    
                    it("should not display forecast") {
                        // Given
                        self.load(view: self.sut.view, in: self.window)
                        
                        // When
                        self.sut.displayForecast(viewModel: viewModel)
                        
                        // Then
                        let forecastOverviewView = self.sut.forecastOverviewView
                        let hourlyForecastDataStore = self.sut.hourlyForecastViewController?.router?.dataStore
                        let dailyForecastDataStore = self.sut.dailyForecastViewController?.router?.dataStore
                        
                        expect(forecastOverviewView!.data?.currentTemperature).to(beNil())
                        expect(hourlyForecastDataStore!.forecast).to(beNil())
                        expect(dailyForecastDataStore!.forecast).to(beNil())
                    }
                }
            }
        }
    }
    
    // MARK: Test doubles
    
    class CityForecastInteractorInterfaceSpy: CityForecastInteractorInterface {
        var fetchForecastTypeWasCalled = false
        var fetchForecastWasCalled = false
        
        var fetchForecastTypeRequest: CityForecastModels.FetchForecastType.Request?
        var fetchForecastRequest: CityForecastModels.FetchForecast.Request?
        
        func fetchForecastType(request: CityForecastModels.FetchForecastType.Request) {
            fetchForecastTypeWasCalled = true
            fetchForecastTypeRequest = request
        }
        
        func fetchForecast(request: CityForecastModels.FetchForecast.Request) {
            fetchForecastWasCalled = true
            fetchForecastRequest = request
        }
    }
}
