//
//  CityForecastPresenterTests.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 20/01/2019.
//  Copyright (c) 2019 Martin Pastorin. All rights reserved.
//

@testable import TheWeatherApp
import Quick
import Nimble

class CityForecastPresenterTests: QuickSpec {

    // MARK: Subject under test
    
    var sut: CityForecastPresenter!

    override func spec() {
        describe("The CityForecast Presenter") {

            beforeEach {
                self.sut = CityForecastPresenter()
            }

            describe("on requesting forecast type") {
                context("with a succesful response") {
                    // Given
                    let forecastType = Seeds.ForecastTypes.default
                    let response = CityForecastModels.FetchForecastType.Response(forecastType: forecastType)
                    
                    it("should call viewController to display forecast type") {
                        // Given
                        let spy = CityForecastDisplayInterfaceSpy()
                        self.sut.viewController = spy
                        
                        // When
                        self.sut.presentForecastType(response: response)
                        
                        // Then
                        expect(spy.displayForecastTypeWasCalled).to(beTrue())
                        expect(spy.displayForecastTypeViewModel!.forecastOverview!.locationName).to(equal(forecastType.locationName))
                    }
                }
            }
            
            describe("on requesting forecast") {
                context("with a succesful response") {
                    // Given
                    let forecastType = Seeds.ForecastTypes.default
                    let forecast = Seeds.Forecasts.default
                    let response = CityForecastModels.FetchForecast.Response(forecastType: forecastType, forecast: forecast, error: nil)
                    
                    it("should call viewController to display forecast") {
                        // Given
                        let spy = CityForecastDisplayInterfaceSpy()
                        self.sut.viewController = spy
                        
                        // When
                        self.sut.presentForecast(response: response)
                        
                        // Then
                        let temperatureRedable = Seeds.Temperatures.celciusDefaultRedable
                        let windRedable = Seeds.Winds.defaultRedable
                        let dateRedable = Seeds.Dates.defaultRedable
                        let iconRedable = Seeds.IconCode.defaultRedable
                        let weatherRedable = Seeds.WeatherDescription.defaultRedable
                        let humidityRedable = Seeds.Humidity.defaultRedable
                        let precipitationRedable = Seeds.Precipitation.defaultRedable
                        let visibilityRedable = Seeds.Visibility.defaultRedable
                        
                        expect(spy.displayForecastWasCalled).to(beTrue())
                        expect(spy.displayForecastViewModel!.error).to(beNil())
                        expect(spy.displayForecastViewModel!.forecastOverview!.locationName).to(equal(forecastType.locationName))
                        expect(spy.displayForecastViewModel!.forecastOverview!.date).to(equal(dateRedable))
                        expect(spy.displayForecastViewModel!.forecastOverview!.iconName).to(equal(iconRedable))
                        expect(spy.displayForecastViewModel!.forecastOverview!.weatherDescription).to(equal(weatherRedable))
                        expect(spy.displayForecastViewModel!.forecastOverview!.currentTemperature).to(equal(temperatureRedable.current))
                        expect(spy.displayForecastViewModel!.forecastOverview!.feelsLike).to(equal(temperatureRedable.feelsLike))
                        expect(spy.displayForecastViewModel!.forecastOverview!.minTemperature).to(equal(temperatureRedable.min))
                        expect(spy.displayForecastViewModel!.forecastOverview!.maxTemperature).to(equal(temperatureRedable.max))
                        expect(spy.displayForecastViewModel!.forecastOverview!.humidity).to(equal(humidityRedable))
                        expect(spy.displayForecastViewModel!.forecastOverview!.precipitation).to(equal(precipitationRedable))
                        expect(spy.displayForecastViewModel!.forecastOverview!.visibility).to(equal(visibilityRedable))
                        expect(spy.displayForecastViewModel!.forecastOverview!.wind).to(equal(windRedable))
                    }
                }
                
                context("with forecastType and an error in the response") {
                    // Given
                    let forecastType = Seeds.ForecastTypes.default
                    let error = Seeds.Error.serverNotFound
                    let response = CityForecastModels.FetchForecast.Response(forecastType: forecastType, forecast: nil, error: error)
                    
                    it("should call viewController to display forecast") {
                        // Given
                        let spy = CityForecastDisplayInterfaceSpy()
                        self.sut.viewController = spy
                        
                        // When
                        self.sut.presentForecast(response: response)
                        
                        // Then
                        expect(spy.displayForecastWasCalled).to(beTrue())
                        expect(spy.displayForecastViewModel!.forecastOverview!.locationName).to(equal(forecastType.locationName))
                        expect(spy.displayForecastViewModel!.error).toNot(beNil())
                    }
                }
                
                context("without forecastType but with an error in the response") {
                    // Given
                    let error = Seeds.Error.serverNotFound
                    let response = CityForecastModels.FetchForecast.Response(forecastType: nil, forecast: nil, error: error)
                    
                    it("should call viewController to display forecast") {
                        // Given
                        let spy = CityForecastDisplayInterfaceSpy()
                        self.sut.viewController = spy
                        
                        // When
                        self.sut.presentForecast(response: response)
                        
                        // Then
                        expect(spy.displayForecastWasCalled).to(beTrue())
                        expect(spy.displayForecastViewModel!.forecastOverview).to(beNil())
                        expect(spy.displayForecastViewModel!.error).toNot(beNil())
                    }
                }
            }
        }
    }
    
    // MARK: Test doubles
    
    class CityForecastDisplayInterfaceSpy: CityForecastDisplayInterface {
        var displayForecastTypeWasCalled = false
        var displayForecastWasCalled = false
        
        var displayForecastTypeViewModel: CityForecastModels.FetchForecastType.ViewModel?
        var displayForecastViewModel: CityForecastModels.FetchForecast.ViewModel?
        
        func displayForecastType(viewModel: CityForecastModels.FetchForecastType.ViewModel) {
            displayForecastTypeWasCalled = true
            displayForecastTypeViewModel = viewModel
        }
        
        func displayForecast(viewModel: CityForecastModels.FetchForecast.ViewModel) {
            displayForecastWasCalled = true
            displayForecastViewModel = viewModel
        }
    }
}
