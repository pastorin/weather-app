//
//  CityForecastInteractorTests.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 20/01/2019.
//  Copyright (c) 2019 Martin Pastorin. All rights reserved.
//

@testable import TheWeatherApp

import Alamofire
import Quick
import Nimble

class CityForecastInteractorTests: QuickSpec {

    // MARK: Subject under test
    
    var sut: CityForecastInteractor!
    
    override func spec() {
        describe("The CityForecast Interactor") {
            beforeEach {
                self.sut = CityForecastInteractor()
            }

            describe("on fetching ForecastType") {
                // Given
                let request = CityForecastModels.FetchForecastType.Request()
                
                context("without forecast type") {
                    it("should not call presenter") {
                        // Given
                        let spy = CityForecastPresenterInterfaceSpy()
                        self.sut.presenter = spy
                        
                        // When
                        self.sut.fetchForecastType(request: request)
                        
                        // Then
                        expect(spy.presentForecastTypeWasCalled).to(beFalse())
                    }
                }
                
                context("with a forecast type") {
                    it("should call presenter to present that type") {
                        // Given
                        let forecastType = Seeds.ForecastTypes.default
                        let spy = CityForecastPresenterInterfaceSpy()
                        self.sut.presenter = spy
                        self.sut.forecastType = forecastType
                        
                        // When
                        self.sut.fetchForecastType(request: request)
                        
                        // Then
                        expect(spy.presentForecastTypeWasCalled).to(beTrue())
                        expect(spy.presentForecastTypeResponse?.forecastType).to(equal(forecastType))
                    }
                }
            }
            
            describe("on fetching Forecast") {
                // Given
                let request = CityForecastModels.FetchForecast.Request()
                
                context("without forecast type") {
                    it("should not call worker") {
                        // Given
                        let spy = CityForecastWorkerInterfaceSpy()
                        self.sut.worker = spy
                        
                        // When
                        self.sut.fetchForecast(request: request)
                        
                        // Then
                        expect(spy.fetchForecastWasCalled).to(beFalse())
                    }
                    
                    it("should not call presenter") {
                        // Given
                        let spy = CityForecastPresenterInterfaceSpy()
                        self.sut.presenter = spy
                        
                        // When
                        self.sut.fetchForecast(request: request)
                        
                        // Then
                        expect(spy.presentForecastWasCalled).to(beFalse())
                    }
                }
                
                context("with a forecast type") {
                    it("should call worker to fetch forecast") {
                        // Given
                        let forecastType = Seeds.ForecastTypes.default
                        
                        let spy = CityForecastWorkerInterfaceSpy()
                        self.sut.worker = spy
                        self.sut.forecastType = forecastType
                        
                        // When
                        self.sut.fetchForecast(request: request)
                        
                        // Then
                        expect(spy.fetchForecastWasCalled).to(beTrue())
                        expect(spy.fetchForecastWithForecastType).to(equal(forecastType))
                    }
                    
                    it("should call presenter to present forecast") {
                        // Given
                        let forecastType = Seeds.ForecastTypes.default
                        let forecast = Seeds.Forecasts.default
                        
                        let spy = CityForecastPresenterInterfaceSpy()
                        let worker = CityForecastWorkerInterfaceSpy()
                        worker.fetchForecastWithForecast = forecast
                        
                        self.sut.presenter = spy
                        self.sut.worker = worker
                        self.sut.forecastType = forecastType
                        
                        // When
                        self.sut.fetchForecast(request: request)
                        
                        // Then
                        expect(spy.presentForecastWasCalled).to(beTrue())
                        expect(spy.presentForecastResponse!.forecastType).to(equal(forecastType))
                        expect(spy.presentForecastResponse!.error).to(beNil())
                        expect(spy.presentForecastResponse!.forecast?.locationName).to(equal(forecast.locationName))
                    }
                }
                
                context("with a server errro") {
                    it("should call presenter to present forecast") {
                        // Given
                        let forecastType = Seeds.ForecastTypes.default
                        
                        let spy = CityForecastPresenterInterfaceSpy()
                        let worker = CityForecastWorkerInterfaceSpy()
                        
                        self.sut.presenter = spy
                        self.sut.worker = worker
                        self.sut.forecastType = forecastType
                        
                        // When
                        self.sut.fetchForecast(request: request)
                        
                        // Then
                        expect(spy.presentForecastWasCalled).to(beTrue())
                        expect(spy.presentForecastResponse!.forecastType).to(equal(forecastType))
                        expect(spy.presentForecastResponse!.error).toNot(beNil())
                        expect(spy.presentForecastResponse!.forecast).to(beNil())
                    }
                }
            }
        }
    }
    
    // MARK: Test doubles
    
    class CityForecastPresenterInterfaceSpy: CityForecastPresenterInterface {
        var presentForecastTypeWasCalled = false
        var presentForecastWasCalled = false
        
        var presentForecastTypeResponse: CityForecastModels.FetchForecastType.Response?
        var presentForecastResponse: CityForecastModels.FetchForecast.Response?
        
        func presentForecastType(response: CityForecastModels.FetchForecastType.Response) {
            presentForecastTypeWasCalled = true
            presentForecastTypeResponse = response
        }
        
        func presentForecast(response: CityForecastModels.FetchForecast.Response) {
            presentForecastWasCalled = true
            presentForecastResponse = response
        }

    }
    
    class CityForecastWorkerInterfaceSpy: CityForecastWorkerInterface {
        var fetchForecastWasCalled = false
        var fetchForecastWithForecastType: ForecastType?
        var fetchForecastWithForecast: Forecast?
        
        func fetchForecast(for forecastType: ForecastType, completion: @escaping (Result<Forecast>) -> Void) {
            fetchForecastWasCalled = true
            fetchForecastWithForecastType = forecastType
            
            if let forecast = fetchForecastWithForecast {
                completion(.success(forecast))
            } else {
                completion(.failure(Seeds.Error.serverNotFound))
            }
        }
    }
}
