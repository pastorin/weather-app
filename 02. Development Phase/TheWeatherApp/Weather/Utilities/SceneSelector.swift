//
//  SceneSelector.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 08/01/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import UIKit

enum SceneSelector {
    
    /// Sets the initialScene of the application
    static func setInitialScene() {
        setScene(fromStoryboard: .weatherForecasts)
    }
    
    /// Sets a specific scene based on the storyboard provided
    ///
    /// - Parameter storyboard: One of the storyboard values, defined in the Storyboards enum
    /// - showNavigationController: Determines if the UIViewController instantiated from the Scene will be contained in a UINavigationController
    static func setScene(fromStoryboard storyboard: Storyboards, inNavigationController showNavigationController: Bool = false) {
        guard let viewController = UIViewController.from(storyboard: storyboard) else {
            fatalError("Could not initialize UIViewController from storyboard: \(storyboard.rawValue)")
        }
        
        let nextViewController: UIViewController
        
        // reset the root view controller
        if showNavigationController {
            nextViewController = UINavigationController(rootViewController: viewController)
        } else {
            nextViewController = viewController
        }
        
        setViewControllerInKeyWindow(nextViewController)
    }
    
    /// Adds the given UIViewController as the rootViewController of the keyWindow
    ///
    /// - Parameter viewController: The ViewController to display
    private static func setViewControllerInKeyWindow(_ viewController: UIViewController) {
        guard let window = UIApplication.shared.keyWindow else {
            fatalError("Usage of the SceneSelector requires a window to be set through the setWindow() method")
        }
        
        // Clean up the view stack before resetting the rootViewController
        for subview in window.subviews {
            subview.removeFromSuperview()
        }
        
        let transition = CATransition()
        transition.duration = 0.25
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = .fade
        transition.fillMode = .both
        window.layer.add(transition, forKey: nil)
        
        window.rootViewController = viewController
    }
    
    /// Performs .makeKey() on the given window to ensure the UIApplication has a keyWindow
    ///
    /// - Parameter window: UIWindow instance
    static func setWindow(_ window: UIWindow?) {
        guard let window = window else {
            return
        }
        
        window.makeKey()
    }
}
