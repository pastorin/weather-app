//
//  Logger.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 08/01/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import Foundation
import SwiftyBeaver

let log = SwiftyBeaver.self

struct Logger {
    
    static func initialize() {
        #if DEBUG
        let console = ConsoleDestination()
        console.format = "$DHH:mm:ss.SSS$d $C $L - $M \n\t$X"
        console.asynchronously = false
        
        console.levelString.verbose = "SUCCEEDED"
        console.levelString.debug = ""
        
        console.levelColor.verbose = "✅"
        console.levelColor.debug = ""
        console.levelColor.info = "ℹ️"
        console.levelColor.warning = "⚠️"
        console.levelColor.error = "⛔️"
        
        log.addDestination(console)
        #endif
    }
}
