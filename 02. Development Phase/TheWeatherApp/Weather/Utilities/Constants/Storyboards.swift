//
//  Storyboards.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 08/01/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import Foundation

enum Storyboards: String {
    case cityForecast = "CityForecast"
    case hourlyForecast = "HourlyForecast"
    case dailyForecast = "DailyForecast"
    case locationAccess = "LocationAccess"
    case weatherForecasts = "WeatherForecasts"
    case locations = "Locations"
    case search = "Search"
}
