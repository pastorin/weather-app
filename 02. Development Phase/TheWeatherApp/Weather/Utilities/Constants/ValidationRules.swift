//
//  ValidationRules.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 18/01/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import Foundation

enum ValidationRules {
    static let minSearchTextLength = 2
}
