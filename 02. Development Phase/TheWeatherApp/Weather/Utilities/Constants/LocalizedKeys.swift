//
//  LocalizedKeys.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 19/01/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import Foundation

enum LocalizedKeys {
    enum Core {
        static let defaultIconName = "default"
        static let emptyValue = "--"
        static let unknown = "core_label_unknown".localized
        static let now = "core_label_now".localized
        static let delete = "core_label_delete".localized
        static let currentLocation = "core_label_currentLocation".localized
    }
    
    enum ForecastOverview {
        static let feelsLike = "forecastOverview_label_feelsLike".localized
        static let min = "forecastOverview_label_min".localized
        static let max = "forecastOverview_label_max".localized
        static let humidity = "forecastOverview_label_humidity".localized
        static let precipitation = "forecastOverview_label_precipitation".localized
        static let visibility = "forecastOverview_label_visibility".localized
        static let wind = "forecastOverview_label_wind".localized
    }
    
    enum LocationAccess {
        static let title = "locationAccess_label_title".localized
        static let description = "locationAccess_label_description".localized
        static let primaryButton = "locationAccess_button_primary".localized
        static let notAuthorizeTitle = "locationAccess_label_notAuthorizeTitle".localized
        static let notAuthorizeDescription = "locationAccess_label_notAuthorizeDescription".localized
        static let notAuthorizePrimaryButton = "locationAccess_button_notAuthorizePrimary".localized
    }
    
    enum Search {
        static let placeholder = "search_label_placeholder".localized
    }
}

private extension String {
    
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
}
