//
//  WeatherForecastsModels.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 12/01/2019.
//  Copyright (c) 2019 Martin Pastorin. All rights reserved.
//

enum WeatherForecastsModels {
    
    // MARK: Use cases
    enum FetchWeatherForecast {
        
        struct Request {
            
        }
        
        struct Response {
            let allForecastType: [ForecastType]
        }
        
        struct ViewModel {
            let allForecastType: [ForecastType]
        }
    }
}
