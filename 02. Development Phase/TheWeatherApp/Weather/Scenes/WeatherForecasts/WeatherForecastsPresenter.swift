//
//  WeatherForecastsPresenter.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 12/01/2019.
//  Copyright (c) 2019 Martin Pastorin. All rights reserved.
//

import UIKit

protocol WeatherForecastsPresenterInterface {
    func presentWeatherForecast(response: WeatherForecastsModels.FetchWeatherForecast.Response)
}

class WeatherForecastsPresenter: WeatherForecastsPresenterInterface {
    
    // MARK: - Properties
    weak var viewController: WeatherForecastsDisplayInterface?
    
    // MARK: - WeatherForecastsPresenterInterface
    func presentWeatherForecast(response: WeatherForecastsModels.FetchWeatherForecast.Response) {
        let viewModel = WeatherForecastsModels.FetchWeatherForecast.ViewModel(allForecastType: response.allForecastType)
        viewController?.displayWeatherForecast(viewModel: viewModel)
    }
}
