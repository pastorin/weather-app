//
//  WeatherForecastsViewController.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 12/01/2019.
//  Copyright (c) 2019 Martin Pastorin. All rights reserved.
//

import UIKit

protocol WeatherForecastsDisplayInterface: class {
    func displayWeatherForecast(viewModel: WeatherForecastsModels.FetchWeatherForecast.ViewModel)
}

class WeatherForecastsViewController: WAViewController {
    
    // MARK: - Outlets
    @IBOutlet private(set) weak var menuButton: UIButton!
    
    // MARK: - Properties
    var interactor: WeatherForecastsInteractorInterface?
    var router: (WeatherForecastsRouterInterface & WeatherForecastsDataPassing)?
    var pageViewController: UIPageViewController!
    
    var allForecastsType = [ForecastType]() {
        didSet {
            updatePageViewController()
        }
    }
    
    // MARK: - Init
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupVIPCycle()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupVIPCycle()
    }
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        performFetchWeatherForecastRequest()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let controller = segue.destination as? UIPageViewController {
            pageViewController = controller
            pageViewController.dataSource = self
            pageViewController.delegate = self
        }
    }
    
    // MARK: - Setup
    private func setupVIPCycle() {
        let viewController = self
        let interactor = WeatherForecastsInteractor()
        let presenter = WeatherForecastsPresenter()
        let router = WeatherForecastsRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }
    
    private func setupUI() {
        
    }
    
    // MARK: - IBAction
    @IBAction private func didTapMenuButton() {
        router?.routeToWeatherCities()
    }
    
    // MARK: - Public
    func setCurrentForecastType(as forecastType: ForecastType, animated: Bool = false) {
        // Check if the forecast exists in the array
        guard allForecastsType.firstIndex(of: forecastType) != nil else {
            return
        }
        
        let viewController = createViewController(for: forecastType)
        pageViewController.setViewControllers([viewController], direction: .forward, animated: animated, completion: nil)
    }
    
    // MARK: - Helpers
    private func performFetchWeatherForecastRequest() {
        let request = WeatherForecastsModels.FetchWeatherForecast.Request()
        interactor?.fetchWeatherForecast(request: request)
    }
    
    private func updatePageViewController() {
        // Check if there is any forecast to display
        guard let firstForecastType = allForecastsType.first else {
            pageViewController.setViewControllers(nil, direction: .forward, animated: true, completion: nil)
            return
        }
        
        // Check if there is a view controller already displayed
        guard let viewController = pageViewController.viewControllers?.first else {
            setCurrentForecastType(as: firstForecastType, animated: true)
            return
        }
        
        // Fetch current weather forecast and check if it still exists in the array
        guard
            let cityForecastVC = viewController as? CityForecastViewController,
            let displayedForecastType = cityForecastVC.router?.dataStore?.forecastType,
            allForecastsType.firstIndex(of: displayedForecastType) != nil else {
                setCurrentForecastType(as: firstForecastType, animated: false)
                return
        }
        
        pageViewController.setViewControllers([cityForecastVC], direction: .forward, animated: false, completion: nil)
    }
    
    fileprivate func forecastType(before forecastType: ForecastType) -> ForecastType? {
        guard let index = allForecastsType.firstIndex(of: forecastType) else {
            return nil
        }
        
        let previousIndex = allForecastsType.index(before: index)
        return previousIndex >= 0 ? allForecastsType[previousIndex] : nil
    }
    
    fileprivate func forecastType(after forecastType: ForecastType) -> ForecastType? {
        guard let index = allForecastsType.firstIndex(of: forecastType) else {
            return nil
        }
        
        let nextIndex = allForecastsType.index(after: index)
        return nextIndex < allForecastsType.count ? allForecastsType[nextIndex] : nil
    }
    
    fileprivate func createViewController(for forecastType: ForecastType) -> UIViewController {
        // Check if it's need to request Location Access
        if case let .locationCoordinate(coordinate) = forecastType, coordinate == nil {
            let viewController = UIViewController.from(storyboard: .locationAccess) as! LocationAccessViewController
            return viewController
        } else {
            let viewController = UIViewController.from(storyboard: .cityForecast) as! CityForecastViewController
            var viewControllerDataStore = viewController.router?.dataStore
            viewControllerDataStore?.forecastType = forecastType
            return viewController
        }
    }
}

// MARK: - WeatherForecastsDisplayInterface
extension WeatherForecastsViewController: WeatherForecastsDisplayInterface {
    
    func displayWeatherForecast(viewModel: WeatherForecastsModels.FetchWeatherForecast.ViewModel) {
        allForecastsType = viewModel.allForecastType
    }
}

// MARK: - UIPageViewControllerDataSource
extension WeatherForecastsViewController: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        // If it is LocationAccessViewController it means that it's the first VC
        if (viewController as? LocationAccessViewController) != nil {
            return nil
        }
        
        // Fetch previous weather forecast
        guard
            let viewController = viewController as? CityForecastViewController,
            let displayedForecastType = viewController.router?.dataStore?.forecastType,
            let previousForecastType = forecastType(before: displayedForecastType) else {
                return nil
        }
    
        return createViewController(for: previousForecastType)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        // If it is LocationAccessViewController it means that it's the first VC
        if (viewController as? LocationAccessViewController) != nil {
            guard
                let displayedForecastType = allForecastsType.first,
                let nextForecastType = forecastType(after: displayedForecastType) else {
                    return nil
            }
            return createViewController(for: nextForecastType)
        }
        
        // Fetch next weather forecast
        guard
            let viewController = viewController as? CityForecastViewController,
            let displayedForecastType = viewController.router?.dataStore?.forecastType,
            let nextForecastType = forecastType(after: displayedForecastType)  else {
                return nil
        }
        
        return createViewController(for: nextForecastType)
    }
    
}

// MARK: - UIPageViewControllerDelegate
extension WeatherForecastsViewController: UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if completed {
//            updatePageControl()
        }
    }
}
