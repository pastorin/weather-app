//
//  WeatherForecastsWorker.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 12/01/2019.
//  Copyright (c) 2019 Martin Pastorin. All rights reserved.
//

import CoreLocation

protocol WeatherForecastsWorkerInterface {
    func fetchAllForecastType(completion: @escaping ([ForecastType]) -> Void)
}

class WeatherForecastsWorker: WeatherForecastsWorkerInterface {
    
    func fetchAllForecastType(completion: @escaping ([ForecastType]) -> Void) {
        completion(WeatherDataManager.sharedManager.allForecastType)
    }
}
