//
//  WeatherForecastsInteractor.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 12/01/2019.
//  Copyright (c) 2019 Martin Pastorin. All rights reserved.
//

import CoreLocation

protocol WeatherForecastsInteractorInterface {
    func fetchWeatherForecast(request: WeatherForecastsModels.FetchWeatherForecast.Request)
}

protocol WeatherForecastsDataStore {
    
}

class WeatherForecastsInteractor: WeatherForecastsInteractorInterface, WeatherForecastsDataStore {
   
    // MARK: - Properties
    var presenter: WeatherForecastsPresenterInterface?
    var worker: WeatherForecastsWorkerInterface = WeatherForecastsWorker()
    
    // MARK: - Initializer
    init() {
        WeatherDataManager.sharedManager.addObserver(self)
    }
    
    // MARK: - WeatherForecastsInteractorInterface
    func fetchWeatherForecast(request: WeatherForecastsModels.FetchWeatherForecast.Request) {
        fetchWeatherForecast()
    }
    
    // MARK: - Helpers
    private func fetchWeatherForecast() {
        worker.fetchAllForecastType { [weak self] allForecastType in
            let response = WeatherForecastsModels.FetchWeatherForecast.Response(allForecastType: allForecastType)
            self?.presenter?.presentWeatherForecast(response: response)
        }
    }
    
}

extension WeatherForecastsInteractor: WeatherDataManagerObserver {
    func weatherDataManager(_ manager: WeatherDataManager, didAddLocation location: String) {
        fetchWeatherForecast()
    }
    
    func weatherDataManager(_ manager: WeatherDataManager, didRemoveLocation location: String) {
        fetchWeatherForecast()
    }
    
    func weatherDataManager(_ manager: WeatherDataManager, didChangeLocation coordinate: CLLocationCoordinate2D?) {
        fetchWeatherForecast()
    }
}
