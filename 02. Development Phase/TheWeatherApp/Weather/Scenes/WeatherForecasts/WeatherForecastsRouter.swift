//
//  WeatherForecastsRouter.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 12/01/2019.
//  Copyright (c) 2019 Martin Pastorin. All rights reserved.
//

import UIKit

protocol WeatherForecastsRouterInterface {
    func routeToWeatherCities()
}

protocol WeatherForecastsDataPassing {
    var dataStore: WeatherForecastsDataStore? { get }
}

class WeatherForecastsRouter: WeatherForecastsRouterInterface, WeatherForecastsDataPassing {
    
    // MARK: - Properties
    weak var viewController: WeatherForecastsViewController?
    var dataStore: WeatherForecastsDataStore?
    
    // MARK: - WeatherForecastsRouterInterface
    func routeToWeatherCities() {
        guard let destination = UIViewController.from(storyboard: .locations) else {
            return
        }
        viewController?.present(destination, animated: true, completion: nil)
    }
}
