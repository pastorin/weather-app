//
//  LocationAccessRouter.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 09/01/2019.
//  Copyright (c) 2019 Martin Pastorin. All rights reserved.
//

import UIKit

protocol LocationAccessRouterInterface {
    func routeToSettings()
}

class LocationAccessRouter: LocationAccessRouterInterface {
    
    // MARK: - Properties
    weak var viewController: LocationAccessViewController?
    
    // MARK: - LocationAccessRouterInterface
    func routeToSettings() {
        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
            return
        }
        UIApplication.shared.open(settingsUrl)
    }
}
