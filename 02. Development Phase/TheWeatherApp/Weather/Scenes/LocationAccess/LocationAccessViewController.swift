//
//  LocationAccessViewController.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 09/01/2019.
//  Copyright (c) 2019 Martin Pastorin. All rights reserved.
//

import UIKit
import CoreLocation

class LocationAccessViewController: WAViewController {
    
    // MARK: - Outlets
    @IBOutlet private(set) weak var imageView: UIImageView!
    @IBOutlet private(set) weak var titleLabel: UILabel!
    @IBOutlet private(set) weak var descriptionLabel: UILabel!
    @IBOutlet private(set) weak var primaryButton: UIButton!
    
    // MARK: - Properties
    var router: LocationAccessRouterInterface?
    let locationManager = CLLocationManager()
    
    // MARK: - Init
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupVIPCycle()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupVIPCycle()
    }
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        checkLocationAccessState()
    }
    
    // MARK: - Setup
    private func setupVIPCycle() {
        let viewController = self
        let router = LocationAccessRouter()
        viewController.router = router
        router.viewController = viewController
    }
    
    private func setupUI() {
        view.backgroundColor = Style.Color.green
        imageView.image = UIImage(named: "location_access")
        titleLabel.setStyle(.bold(color: Style.Color.green))
        descriptionLabel.setStyle(.light(color: Style.Color.green))
        primaryButton.setStyle(.green)
    }
    
    // MARK: - IBAction
    @IBAction private func didTapPrimaryButton() {
        switch CLLocationManager.authorizationStatus() {
        case .notDetermined:
            requestLocationAccess()
        case .denied, .restricted:
            router?.routeToSettings()
        default:
            break
        }
    }
    
    // MARK: - Callbacks
    @objc
    private func appWillEnterForeground() {
        checkLocationAccessState()
    }
    
    // MARK: - Helpers
    fileprivate func checkLocationAccessState() {
        // If the authorization is not determined, ask for permission
        guard CLLocationManager.authorizationStatus() != .notDetermined else {
            titleLabel.text = LocalizedKeys.LocationAccess.title.uppercased()
            descriptionLabel.text = LocalizedKeys.LocationAccess.description
            primaryButton.setTitle(LocalizedKeys.LocationAccess.primaryButton.uppercased(), for: .normal, animated: false)
            return
        }
        
        // If user did not authorize, request to go to settings
        if CLLocationManager.authorizationStatus() != .authorizedWhenInUse {
            titleLabel.text = LocalizedKeys.LocationAccess.notAuthorizeTitle.uppercased()
            descriptionLabel.text = LocalizedKeys.LocationAccess.notAuthorizeDescription
            primaryButton.setTitle(LocalizedKeys.LocationAccess.notAuthorizePrimaryButton.uppercased(), for: .normal, animated: false)
            return
        }
    }
    
    private func requestLocationAccess() {
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
    }
}

// MARK: - CLLocationManagerDelegate
extension LocationAccessViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        checkLocationAccessState()
    }
}
