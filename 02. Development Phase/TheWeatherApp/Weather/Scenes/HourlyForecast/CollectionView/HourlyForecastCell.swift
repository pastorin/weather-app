//
//  HourlyForecastCell.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 08/01/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import UIKit

protocol HourlyForecastCellViewModel {
    var hour: String { get }
    var temperature: String { get }
    var iconName: String { get }
    var isNow: Bool { get }
}

class HourlyForecastCell: UICollectionViewCell {
    
    // MARK: - Outlets
    @IBOutlet private(set) weak var iconImageView: UIImageView!
    @IBOutlet private(set) weak var hourLabel: UILabel!
    @IBOutlet private(set) weak var hourView: UIView!
    @IBOutlet private(set) weak var temperatureLabel: UILabel!
    
    // MARK: - Properties
    var data: HourlyForecastCellViewModel? {
        didSet {
            updateDataContent()
        }
    }
    
    // MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    // MARK: - Helpers
    private func setupUI() {
        hourLabel.setStyle(.regular(color: Style.Color.white))
        temperatureLabel.setStyle(.bold(color: Style.Color.white))
        
        hourView.backgroundColor = Style.Color.white.withAlphaComponent(Style.Alpha.medium)
        hourView.layer.cornerRadius = hourView.layer.frame.height / 2
        hourView.layer.masksToBounds = true
    }
    
    private func updateDataContent() {
        guard let data = data else {
            return
        }
        
        hourLabel.text = data.isNow ? LocalizedKeys.Core.now.uppercased() : data.hour
        temperatureLabel.text = data.temperature
        iconImageView.image = UIImage(named: data.iconName)
        
        hourLabel.setStyle(data.isNow ? .bold(color: Style.Color.white) : .regular(color: Style.Color.white))
    }
}
