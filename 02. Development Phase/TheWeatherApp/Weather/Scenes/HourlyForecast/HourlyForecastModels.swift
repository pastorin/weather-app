//
//  HourlyForecastModels.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 08/01/2019.
//  Copyright (c) 2019 Martin Pastorin. All rights reserved.
//

enum HourlyForecastModels {
    
    // MARK: Use cases
    enum FetchHourlyForecast {
        
        struct Request {
            
        }
        
        struct Response {
            var forecast: [HourlyForecast]?
        }
        
        struct ViewModel {
            var hourlyForecast = [HourlyForecastCellViewModel]()

            struct Item: HourlyForecastCellViewModel {
                let hour: String
                let temperature: String
                var iconName: String
                var isNow: Bool
            }
        }
    }
}
