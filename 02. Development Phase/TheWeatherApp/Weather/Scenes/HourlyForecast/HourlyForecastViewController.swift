//
//  HourlyForecastViewController.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 08/01/2019.
//  Copyright (c) 2019 Martin Pastorin. All rights reserved.
//

import UIKit

protocol HourlyForecastDisplayInterface: class {
    func displayHourlyForecast(viewModel: HourlyForecastModels.FetchHourlyForecast.ViewModel)
}

class HourlyForecastViewController: WAViewController {
    
    // MARK: - Outlets
    @IBOutlet private(set) var collectionView: UICollectionView!
    
    // MARK: - Properties
    var interactor: HourlyForecastInteractorInterface?
    var router: (HourlyForecastRouterInterface & HourlyForecastDataPassing)?
    
    var data = [HourlyForecastCellViewModel]() {
        didSet {
            collectionView.reloadData()
        }
    }
    
    // MARK: - Init
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupVIPCycle()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupVIPCycle()
    }
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        performRequest()
    }
    
    // MARK: - Setup
    private func setupVIPCycle() {
        let viewController = self
        let interactor = HourlyForecastInteractor()
        let presenter = HourlyForecastPresenter()
        let router = HourlyForecastRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }
    
    private func setupUI() {
        collectionView.backgroundColor = Style.Color.white.withAlphaComponent(Style.Alpha.heavy)
        collectionView.allowsSelection = false
    }
    
    // MARK: - Helpers
    private func performRequest() {
        let request = HourlyForecastModels.FetchHourlyForecast.Request()
        interactor?.fetchHourlyForecast(request: request)
    }
}

// MARK: - HourlyForecastDisplayInterface
extension HourlyForecastViewController: HourlyForecastDisplayInterface {
    
    func displayHourlyForecast(viewModel: HourlyForecastModels.FetchHourlyForecast.ViewModel) {
        data = viewModel.hourlyForecast
    }
}

// MARK: - UICollectionViewDataSource
extension HourlyForecastViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: HourlyForecastCell.self), for: indexPath) as! HourlyForecastCell
        cell.data = data[indexPath.row]
        return cell
    }
}
