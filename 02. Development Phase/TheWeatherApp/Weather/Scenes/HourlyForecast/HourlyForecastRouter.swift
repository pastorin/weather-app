//
//  HourlyForecastRouter.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 08/01/2019.
//  Copyright (c) 2019 Martin Pastorin. All rights reserved.
//

import UIKit

protocol HourlyForecastRouterInterface {
    
}

protocol HourlyForecastDataPassing {
    var dataStore: HourlyForecastDataStore? { get }
}

class HourlyForecastRouter: HourlyForecastRouterInterface, HourlyForecastDataPassing {
    
    // MARK: - Properties
    weak var viewController: HourlyForecastViewController?
    var dataStore: HourlyForecastDataStore?
}
