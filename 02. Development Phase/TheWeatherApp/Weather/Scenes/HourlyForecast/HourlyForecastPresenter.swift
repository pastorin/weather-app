//
//  HourlyForecastPresenter.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 08/01/2019.
//  Copyright (c) 2019 Martin Pastorin. All rights reserved.
//

import UIKit

protocol HourlyForecastPresenterInterface {
    func presentHourlyForecast(response: HourlyForecastModels.FetchHourlyForecast.Response)
}

class HourlyForecastPresenter: HourlyForecastPresenterInterface {
    
    // MARK: - Properties
    weak var viewController: HourlyForecastDisplayInterface?
    
    // MARK: - HourlyForecastPresenterInterface
    func presentHourlyForecast(response: HourlyForecastModels.FetchHourlyForecast.Response) {
        var viewModel = HourlyForecastModels.FetchHourlyForecast.ViewModel()
        
        guard let forecast = response.forecast, !forecast.isEmpty else {
            viewController?.displayHourlyForecast(viewModel: viewModel)
            return
        }
        
        // Create an array of HourlyForecast
        var hourlyForecast = forecast.compactMap { item -> HourlyForecastModels.FetchHourlyForecast.ViewModel.Item? in
            guard let currentTemperature = item.celsiusTemperature.current else {
                return nil
            }
            
            return HourlyForecastModels.FetchHourlyForecast.ViewModel.Item(hour: item.date.timeReadable(),
                                                                           temperature: currentTemperature.temperatureReadable(),
                                                                           iconName: item.iconCode.iconFilename(),
                                                                           isNow: false)
        }
        
        // Set isNow to true for the first element
        hourlyForecast[0].isNow = true
        
        viewModel.hourlyForecast = hourlyForecast
        viewController?.displayHourlyForecast(viewModel: viewModel)
    }
}
