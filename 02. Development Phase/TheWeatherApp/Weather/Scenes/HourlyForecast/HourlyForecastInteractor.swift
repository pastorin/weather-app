//
//  HourlyForecastInteractor.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 08/01/2019.
//  Copyright (c) 2019 Martin Pastorin. All rights reserved.
//

protocol HourlyForecastInteractorInterface {
    func fetchHourlyForecast(request: HourlyForecastModels.FetchHourlyForecast.Request)
}

protocol HourlyForecastDataStore {
    var forecast: Forecast? { get set }
}

class HourlyForecastInteractor: HourlyForecastInteractorInterface, HourlyForecastDataStore {
   
    // MARK: - Properties
    var presenter: HourlyForecastPresenterInterface?
    var forecast: Forecast? {
        didSet {
            let request = HourlyForecastModels.FetchHourlyForecast.Request()
            fetchHourlyForecast(request: request)
        }
    }
    
    // MARK: - HourlyForecastInteractorInterface
    func fetchHourlyForecast(request: HourlyForecastModels.FetchHourlyForecast.Request) {
        var response = HourlyForecastModels.FetchHourlyForecast.Response()
        
        guard let forecast = forecast else {
            presenter?.presentHourlyForecast(response: response)
            return
        }
        
        response.forecast = forecast.hourlyForecast
        presenter?.presentHourlyForecast(response: response)
    }
}
