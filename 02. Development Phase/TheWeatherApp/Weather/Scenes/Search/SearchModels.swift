//
//  SearchModels.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 16/01/2019.
//  Copyright (c) 2019 Martin Pastorin. All rights reserved.
//

enum SearchModels {
    
    // MARK: Use cases
    enum FetchLocations {
        
        struct Request {
            let searchText: String
        }
        
        struct Response {
            var locations: [APILocation]?
            var error: Error?
        }
        
        struct ViewModel {
            var locations: [String]?
            var error: Error?
            var errorMessage: String? {
                return error?.localizedDescription
            }
            
            var shouldDisplayErrorInEmptyView: Bool {
                return error as? APIError != nil
            }
            
        }
    }
    
    enum AddLocation {
        
        struct Request {
            let location: String
        }
        
        struct Response {
            
        }
        
        struct ViewModel {
            
        }
    }
}
