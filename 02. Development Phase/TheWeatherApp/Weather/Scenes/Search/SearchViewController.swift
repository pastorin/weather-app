//
//  SearchViewController.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 16/01/2019.
//  Copyright (c) 2019 Martin Pastorin. All rights reserved.
//

import UIKit

protocol SearchDisplayInterface: class {
    func displayLocations(viewModel: SearchModels.FetchLocations.ViewModel)
}

class SearchViewController: UIViewController {
    
    // MARK: - IBOutlet
    @IBOutlet private(set) weak var searchBarContainer: UIView!
    @IBOutlet private(set) weak var searchBar: UISearchBar!
    @IBOutlet private(set) weak var tableView: UITableView!
    
    @IBOutlet private(set) var searchBarBottomConstraint: NSLayoutConstraint!
    @IBOutlet private(set) var searchBarTopConstraint: NSLayoutConstraint!
    
    // MARK: - Properties
    private var locations = [String]() {
        didSet {
            tableView.reloadData()
        }
    }
    
    var interactor: SearchInteractorInterface?
    var router: (SearchRouterInterface & SearchDataPassing)?
    let emptyView: SearchEmptyView = .fromNib()
    
    // MARK: - Init
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupVIPCycle()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupVIPCycle()
    }
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        showSearchBar(false, animated: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        showSearchBar(true, animated: true)
        searchBar.becomeFirstResponder()
    }
    
    // MARK: - Setup
    private func setupVIPCycle() {
        let viewController = self
        let interactor = SearchInteractor()
        let presenter = SearchPresenter()
        let router = SearchRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }
    
    private func setupUI() {
        view.isOpaque = false
        view.backgroundColor = .clear
    
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = Style.Color.gray.withAlphaComponent(Style.Alpha.heavy)
        
        searchBar.placeholder = LocalizedKeys.Search.placeholder
        searchBar.showsCancelButton = true
        searchBar.tintColor = Style.Color.gray
    }
    
    // MARK: - Helpers
    private func performFetchLocationsRequest(for searchText: String) {
        showEmptyState(false)
        let request = SearchModels.FetchLocations.Request(searchText: searchText)
        interactor?.fetchLocations(request: request)
    }

    private func performAddLocationsRequest(for location: String) {
        let request = SearchModels.AddLocation.Request(location: location)
        interactor?.addLocation(request: request)
    }
    
    private func showSearchBar(_ show: Bool, animated: Bool, completion: (() -> Void)? = nil) {
        let duration = animated ? 0.2 : 0
        
        searchBarTopConstraint.isActive = show
        searchBarBottomConstraint.isActive = !show
        
        if show {
            searchBarContainer.alpha = 1
            tableView.alpha = 0
        }
        
        UIView.animate(withDuration: duration,
                       animations: {
                        self.tableView.alpha = show ? 1 : 0
                        self.view.layoutIfNeeded()
                       },
                       completion: {  _ in
                        self.searchBarContainer.alpha = show ? 1 : 0
                        completion?()
        })
    }
    
    private func showEmptyState(_ show: Bool, message: String? = nil) {
        guard let message = message, show else {
            tableView.backgroundView = nil
            return
        }
        
        emptyView.titleLabel.text = message
        tableView.backgroundView = emptyView
    }
    
    private func dismissSearchController() {
        searchBar.resignFirstResponder()
        locations = []
        showSearchBar(false, animated: true) {
            self.router?.routeToWeatherCities()
        }
    }
}

// MARK: - SearchDisplayInterface
extension SearchViewController: SearchDisplayInterface {
    
    func displayLocations(viewModel: SearchModels.FetchLocations.ViewModel) {
        if viewModel.shouldDisplayErrorInEmptyView {
            showEmptyState(true, message: viewModel.errorMessage)
        }
        
        locations = viewModel.locations ?? []
    }
    
}

// MARK: - UISearchBarDelegate
extension SearchViewController: UISearchBarDelegate {
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        dismissSearchController()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        performFetchLocationsRequest(for: searchText)
    }
    
}

// MARK: - UITableViewDataSource
extension SearchViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return locations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.setStyle(.regular(color: Style.Color.gray))
        cell.textLabel?.text = locations[indexPath.row]
        return cell
    }
    
}

// MARK: - UITableViewDelegate
extension SearchViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let location = locations[indexPath.row]
        performAddLocationsRequest(for: location)
        dismissSearchController()
    }
    
}
