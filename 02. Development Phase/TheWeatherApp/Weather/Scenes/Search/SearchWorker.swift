//
//  SearchWorker.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 16/01/2019.
//  Copyright (c) 2019 Martin Pastorin. All rights reserved.
//

import Alamofire

protocol SearchWorkerInterface {
    func search(for searchText: String, completion: @escaping (APIResult<SearchResult>) -> Void)
    func addLocation(_ location: String)
}

class SearchWorker: SearchWorkerInterface {
    
    // MARK: - Definition
    typealias CompletionHandler = (APIResult<SearchResult>) -> Void
    
    // MARK: - Properties
    private let searchTextKey = "searchText"
    private let completionKey = "completion"
    
    var dataRequest: DataRequest?
    var searchRequestTimer: Timer?
    
    // MARK: - SearchWorkerInterface
    func search(for searchText: String, completion: @escaping (APIResult<SearchResult>) -> Void) {
        // If there is any request in progress, cancel it
        if dataRequest != nil {
            dataRequest?.cancel()
            dataRequest = nil
        }
        
        // Set timer to perform search request in 0.0005 seconds
        searchRequestTimer?.invalidate()
        searchRequestTimer = Timer.scheduledTimer(timeInterval: 0.5,
                                                  target: self,
                                                  selector: #selector(performSearchRequest),
                                                  userInfo: [searchTextKey: searchText, completionKey: completion],
                                                  repeats: false)
    }
    
    func addLocation(_ location: String) {
        WeatherDataManager.sharedManager.addLocation(location)
    }

    // MARK: - Helpers
    @objc
    private func performSearchRequest() {
        guard
            let userInfo = searchRequestTimer?.userInfo as? [String: Any],
            let searchText = userInfo[searchTextKey] as? String,
            let completion = userInfo[completionKey] as? CompletionHandler else {
                return
        }
        
        // Stop Timer
        searchRequestTimer?.invalidate()
        searchRequestTimer = nil
        
        dataRequest = SearchService.search(for: searchText) { result in
            if let error = result.error as NSError?, error.code == NSURLErrorCancelled {
                return
            }
            completion(result)
        }
    }
}
