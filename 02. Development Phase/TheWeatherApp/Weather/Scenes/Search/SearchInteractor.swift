//
//  SearchInteractor.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 16/01/2019.
//  Copyright (c) 2019 Martin Pastorin. All rights reserved.
//

protocol SearchInteractorInterface {
    func fetchLocations(request: SearchModels.FetchLocations.Request)
    func addLocation(request: SearchModels.AddLocation.Request)
}

protocol SearchDataStore {
    
}

class SearchInteractor: SearchInteractorInterface, SearchDataStore {
   
    // MARK: - Properties
    var presenter: SearchPresenterInterface?
    var worker: SearchWorkerInterface = SearchWorker()
    
    // MARK: - SearchInteractorInterface
    func fetchLocations(request: SearchModels.FetchLocations.Request) {
        var response = SearchModels.FetchLocations.Response()
        
        guard request.searchText.count > ValidationRules.minSearchTextLength else {
            presenter?.presentLocations(response: response)
            return
        }
        
        worker.search(for: request.searchText) { [weak self] result in
            switch result {
            case .success(let searchResult):
                response.locations = searchResult.locations
            case .failure(let error):
                response.error = error
            }
            
            self?.presenter?.presentLocations(response: response)
        }
    }
    
    func addLocation(request: SearchModels.AddLocation.Request) {
        worker.addLocation(request.location)
    }
}
