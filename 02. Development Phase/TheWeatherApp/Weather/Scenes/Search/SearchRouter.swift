//
//  SearchRouter.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 16/01/2019.
//  Copyright (c) 2019 Martin Pastorin. All rights reserved.
//

import UIKit

protocol SearchRouterInterface {
    func routeToWeatherCities()
}

protocol SearchDataPassing {
    var dataStore: SearchDataStore? { get }
}

class SearchRouter: SearchRouterInterface, SearchDataPassing {
    
    // MARK: - Properties
    weak var viewController: SearchViewController?
    var dataStore: SearchDataStore?
    
    // MARK: - SearchRouterInterface
    func routeToWeatherCities() {
        viewController?.dismiss(animated: true, completion: nil)
    }
}
