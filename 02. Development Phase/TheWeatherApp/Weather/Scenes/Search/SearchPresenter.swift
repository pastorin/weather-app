//
//  SearchPresenter.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 16/01/2019.
//  Copyright (c) 2019 Martin Pastorin. All rights reserved.
//

import UIKit

protocol SearchPresenterInterface {
    func presentLocations(response: SearchModels.FetchLocations.Response)
}

class SearchPresenter: SearchPresenterInterface {
    
    // MARK: - Properties
    weak var viewController: SearchDisplayInterface?
    
    // MARK: - SearchPresenterInterface
    func presentLocations(response: SearchModels.FetchLocations.Response) {
        var viewModel = SearchModels.FetchLocations.ViewModel()
        viewModel.error = response.error
        viewModel.locations = response.locations?.map { "\($0.areaName), \($0.country)" }
        viewController?.displayLocations(viewModel: viewModel)
    }
}
