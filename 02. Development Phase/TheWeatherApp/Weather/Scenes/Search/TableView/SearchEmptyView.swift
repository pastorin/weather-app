//
//  SearchEmptyView.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 20/01/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import UIKit

class SearchEmptyView: UIView {
    // MARK: - Outlets
    @IBOutlet private(set) weak var titleLabel: UILabel!
    
    // MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    // MARK: - Setup
    private func setupUI() {
        titleLabel.setStyle(.regular(color: Style.Color.gray))
    }
}
