//
//  LocationsWorker.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 13/01/2019.
//  Copyright (c) 2019 Martin Pastorin. All rights reserved.
//

import CoreLocation

protocol LocationsWorkerInterface {
    func fetchAllWeatherForecast(completion: @escaping ([WeatherForecast]) -> Void)
    func fetchForecast(for forecastType: ForecastType, completion: @escaping (APIResult<Forecast>) -> Void)
    func removeLocation(_ location: String)
}

class LocationsWorker: LocationsWorkerInterface {
    
    // MARK: - Definition
    typealias CompletionHandler = (APIResult<Forecast>) -> Void
    
    // MARK: - LocationsWorkerInterface
    func fetchAllWeatherForecast(completion: @escaping ([WeatherForecast]) -> Void) {
        completion(WeatherDataManager.sharedManager.allWeatherForecast)
    }
    
    func fetchForecast(for forecastType: ForecastType, completion: @escaping (APIResult<Forecast>) -> Void) {
        // Check if the forecast was already fetched
        if let forecast = WeatherDataManager.sharedManager.forecast(for: forecastType) {
            completion(.success(forecast))
            return
        }
        
        // Completion handler to storage forecast if it was fetched successfully
        let completionHandler: CompletionHandler = { result in
            if case let .success(forecast) = result {
                WeatherDataManager.sharedManager.updateForecast(forecast, for: forecastType)
            }
            
            completion(result)
        }
        
        // Fetch forecast
        switch forecastType {
        case .locationCoordinate(let coordinate):
            guard let coordinate = coordinate, CLLocationCoordinate2DIsValid(coordinate) else {
                return
            }
            ForecastService.forecast(for: coordinate, completion: completionHandler)
        case .locationName(let locationName):
            ForecastService.forecast(for: locationName, completion: completionHandler)
        }
    }
    
    func removeLocation(_ location: String) {
        WeatherDataManager.sharedManager.removeLocation(location)
    }
}
