//
//  LocationsModels.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 13/01/2019.
//  Copyright (c) 2019 Martin Pastorin. All rights reserved.
//

enum LocationsModels {
    
    // MARK: Use cases
    enum FetchLocations {
        
        struct Request {
            
        }
        
        struct Response {
            let allWeatherForecast: [WeatherForecast]
        }
        
        struct ViewModel {
            var locations = [LocationCellViewModel]()
            
            struct Item: LocationCellViewModel {
                var id: String
                var forecastType: ForecastType
                var locationName: String
                var date: String?
                var iconName: String?
                var currentTemperature: String?
            }
        }
    }
    
    enum RemoveLocation {
        
        struct Request {
            let locationId: String
        }
        
        struct Response {
            
        }
        
        struct ViewModel {
            
        }
    }
}
