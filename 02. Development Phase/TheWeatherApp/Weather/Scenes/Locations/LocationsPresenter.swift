//
//  LocationsPresenter.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 13/01/2019.
//  Copyright (c) 2019 Martin Pastorin. All rights reserved.
//

import UIKit

protocol LocationsPresenterInterface {
    func presentLocations(response: LocationsModels.FetchLocations.Response)
}

class LocationsPresenter: LocationsPresenterInterface {
    
    // MARK: - Properties
    weak var viewController: LocationsDisplayInterface?
    
    // MARK: - WeatherCitiesPresenterInterface
    func presentLocations(response: LocationsModels.FetchLocations.Response) {
        var viewModel = LocationsModels.FetchLocations.ViewModel()
        
        viewModel.locations = response.allWeatherForecast.compactMap {
            LocationsModels.FetchLocations.ViewModel.Item(
                id: $0.id,
                forecastType: $0.forecastType,
                locationName: $0.forecastType.locationName.readableLocationName(),
                date: $0.forecast?.date.readable(),
                iconName: $0.forecast?.iconCode.iconFilename(),
                currentTemperature: $0.forecast?.celsiusTemperature.current?.temperatureReadable())
        }
        
        viewController?.displayLocations(viewModel: viewModel)
    }

}
