//
//  LocationsCell.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 13/01/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import UIKit

protocol LocationCellViewModel: LocationTitleViewModel {
    var id: String { get }
    var forecastType: ForecastType { get }
    var locationName: String { get }
    var date: String? { get }
    var iconName: String? { get }
    var currentTemperature: String? { get }
}

class LocationCell: UITableViewCell {
    
    // MARK: - Outlets
    @IBOutlet private(set) weak var locationTitleView: LocationTitleView!
    @IBOutlet private(set) weak var currentTempLabel: UILabel!
    
    // MARK: - Properties
    var data: LocationCellViewModel? {
        didSet {
            updateDataContent()
        }
    }
    
    // MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    // MARK: - Helpers
    private func setupUI() {
        currentTempLabel.setStyle(.bold(color: Style.Color.white))
    }
    
    private func updateDataContent() {
        guard let data = data else {
            return
        }
        
        locationTitleView.data = data
        currentTempLabel.text = data.currentTemperature ?? LocalizedKeys.Core.emptyValue
    }
}
