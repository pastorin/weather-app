//
//  LocationsViewController.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 13/01/2019.
//  Copyright (c) 2019 Martin Pastorin. All rights reserved.
//

import UIKit

protocol LocationsDisplayInterface: class {
    func displayLocations(viewModel: LocationsModels.FetchLocations.ViewModel)
}

class LocationsViewController: WAViewController {
    
    // MARK: - Outlets
    @IBOutlet private(set) weak var tableView: UITableView!
    @IBOutlet private(set) weak var headerView: UIView!
    
    // MARK: - Properties
    var interactor: LocationsInteractorInterface?
    var router: (LocationsRouterInterface & LocationsDataPassing)?
    
    var data = [LocationCellViewModel]() {
        didSet {
            tableView.reloadData()
        }
    }
    
    // MARK: - Init
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupVIPCycle()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupVIPCycle()
    }
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        performFetchCitiesRequest()
    }
    
    // MARK: - Setup
    private func setupVIPCycle() {
        let viewController = self
        let interactor = LocationsInteractor()
        let presenter = LocationsPresenter()
        let router = LocationsRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }
    
    private func setupUI() {
        tableView.contentInsetAdjustmentBehavior = .never
        headerView.backgroundColor = Style.Color.green
    }
    
    // MARK: - IBAction
    @IBAction private func didTapAddButton() {
        router?.routeToSearch()
    }
    
    // MARK: - Helpers
    private func performFetchCitiesRequest() {
        let request = LocationsModels.FetchLocations.Request()
        interactor?.fetchLocations(request: request)
    }
    
    private func performDeleteLocationRequest(locationId: String) {
        let request = LocationsModels.RemoveLocation.Request(locationId: locationId)
        interactor?.removeLocation(request: request)
    }
}

// MARK: - WeatherCitiesDisplayInterface
extension LocationsViewController: LocationsDisplayInterface {
    
    func displayLocations(viewModel: LocationsModels.FetchLocations.ViewModel) {
        data = viewModel.locations
    }
}

// MARK: - UITableViewDataSource
extension LocationsViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: LocationCell.self)) as! LocationCell
        cell.data = data[indexPath.row]
        cell.selectionStyle = .none
        cell.backgroundColor = Style.Color.green
        return cell
    }
}

// MARK: - UITableViewDelegate
extension LocationsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cellData = data[indexPath.row]
        router?.routeToWeatherForecasts(with: cellData.forecastType)
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        // Not allow to delete current location
        guard indexPath.row > 0 else {
            return UISwipeActionsConfiguration(actions: [])
        }
        
        let deleteAction = UIContextualAction(style: .destructive, title: LocalizedKeys.Core.delete) { [weak self] _, _, _  in
            guard let strongSelf = self else {
                return
            }
            let locationId = strongSelf.data[indexPath.row].id
            strongSelf.performDeleteLocationRequest(locationId: locationId)
        }
        return UISwipeActionsConfiguration(actions: [deleteAction])
    }
}
