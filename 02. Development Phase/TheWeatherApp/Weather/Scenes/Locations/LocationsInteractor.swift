//
//  LocationsInteractor.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 13/01/2019.
//  Copyright (c) 2019 Martin Pastorin. All rights reserved.
//

import CoreLocation

protocol LocationsInteractorInterface {
    func fetchLocations(request: LocationsModels.FetchLocations.Request)
    func removeLocation(request: LocationsModels.RemoveLocation.Request)
}

protocol LocationsDataStore {
    
}

class LocationsInteractor: LocationsInteractorInterface, LocationsDataStore {
   
    // MARK: - Properties
    var presenter: LocationsPresenterInterface?
    var worker: LocationsWorkerInterface = LocationsWorker()
    var allWeatherForecast = [WeatherForecast]()
    var didStartFetchingForecasts = false
    
    // MARK: - Initializer
    init() {
        WeatherDataManager.sharedManager.addObserver(self)
    }
    
    // MARK: - WeatherCitiesInteractorInterface
    func fetchLocations(request: LocationsModels.FetchLocations.Request) {
        fetchLocations()
    }
    
    func removeLocation(request: LocationsModels.RemoveLocation.Request) {
        guard let forecast = allWeatherForecast.first(where: { $0.id == request.locationId }) else {
            return
        }
        worker.removeLocation(forecast.forecastType.locationName)
        fetchLocations()
    }
    
    // MARK: - Helpers
    private func fetchLocations() {
        worker.fetchAllWeatherForecast { [weak self] allWeatherForecast in
            guard let strongSelf = self else {
                return
            }
            
            strongSelf.allWeatherForecast = allWeatherForecast
            
            let response = LocationsModels.FetchLocations.Response(allWeatherForecast: allWeatherForecast)
            strongSelf.presenter?.presentLocations(response: response)
            strongSelf.fetchForecastIfNeeded(for: allWeatherForecast)
        }
    }
    
    private func fetchForecastIfNeeded(for allWeatherForecast: [WeatherForecast]) {
        guard !didStartFetchingForecasts else {
            return
        }
        
        didStartFetchingForecasts = true
        allWeatherForecast.forEach {
            guard $0.forecast == nil else {
                return
            }
            
            let forecastType = $0.forecastType
            worker.fetchForecast(for: forecastType) { [weak self] result in
                if case let .success(forecast) = result {
                    self?.updateForecast(forecast, for: forecastType)
                }
            }
        }
    }
    
    private func updateForecast(_ forecast: Forecast?, for forecastType: ForecastType) {
        guard let index = allWeatherForecast.firstIndex(where: { $0.forecastType == forecastType }) else {
            return
        }
        
        var weatherForecast = allWeatherForecast[index]
        weatherForecast.forecast = forecast
        allWeatherForecast[index] = weatherForecast
        
        let response = LocationsModels.FetchLocations.Response(allWeatherForecast: allWeatherForecast)
        presenter?.presentLocations(response: response)
    }
}

extension LocationsInteractor: WeatherDataManagerObserver {
    
    func weatherDataManager(_ manager: WeatherDataManager, didAddLocation location: String) {
        didStartFetchingForecasts = false
        fetchLocations()
    }
    
    func weatherDataManager(_ manager: WeatherDataManager, didChangeLocation coordinate: CLLocationCoordinate2D?) {
        didStartFetchingForecasts = false
        fetchLocations()
    }
}
