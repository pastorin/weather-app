//
//  LocationsRouter.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 13/01/2019.
//  Copyright (c) 2019 Martin Pastorin. All rights reserved.
//

import UIKit

protocol LocationsRouterInterface {
    func routeToWeatherForecasts(with forecastType: ForecastType)
    func routeToSearch()
}

protocol LocationsDataPassing {
    var dataStore: LocationsDataStore? { get }
}

class LocationsRouter: LocationsRouterInterface, LocationsDataPassing {
    
    // MARK: - Properties
    weak var viewController: LocationsViewController?
    var dataStore: LocationsDataStore?
    
    // MARK: - WeatherCitiesRouterInterface
    func routeToWeatherForecasts(with forecastType: ForecastType) {
        if let presentingViewController = viewController?.presentingViewController as? WeatherForecastsViewController {
            presentingViewController.setCurrentForecastType(as: forecastType)
        }
        viewController?.dismiss(animated: true, completion: nil)
    }
    
    func routeToSearch() {
        guard let destination = UIViewController.from(storyboard: .search) else {
            return
        }
        
        destination.modalPresentationStyle = .overCurrentContext
        viewController?.present(destination, animated: false, completion: nil)
    }
}
