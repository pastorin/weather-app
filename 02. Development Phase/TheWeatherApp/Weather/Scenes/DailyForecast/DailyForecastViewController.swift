//
//  DailyForecastViewController.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 09/01/2019.
//  Copyright (c) 2019 Martin Pastorin. All rights reserved.
//

import UIKit

protocol DailyForecastDisplayInterface: class {
    func displayDailyForecast(viewModel: DailyForecastModels.FetchDailyForecast.ViewModel)
}

class DailyForecastViewController: WAViewController {
    
    // MARK: - Outlets
    @IBOutlet private(set) weak var tableView: UITableView!
    
    // MARK: - Properties
    var interactor: DailyForecastInteractorInterface?
    var router: (DailyForecastRouterInterface & DailyForecastDataPassing)?
    
    var data = [DailyForecastCellViewModel]() {
        didSet {
            tableView.reloadData()
        }
    }
    
    // MARK: - Init
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupVIPCycle()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupVIPCycle()
    }
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        performRequest()
    }
    
    // MARK: - Setup
    private func setupVIPCycle() {
        let viewController = self
        let interactor = DailyForecastInteractor()
        let presenter = DailyForecastPresenter()
        let router = DailyForecastRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }
    
    private func setupUI() {
        view.backgroundColor = Style.Color.white.withAlphaComponent(Style.Alpha.light)
        tableView.backgroundColor = UIColor.clear
        tableView.allowsSelection = false
    }
    
    // MARK: - Helpers
    
    private func performRequest() {
        let request = DailyForecastModels.FetchDailyForecast.Request()
        interactor?.fetchDailyForecast(request: request)
    }
}

// MARK: - DailyForecastDisplayInterface
extension DailyForecastViewController: DailyForecastDisplayInterface {
    
    func displayDailyForecast(viewModel: DailyForecastModels.FetchDailyForecast.ViewModel) {
        data = viewModel.dailyForecast
    }
}

// MARK: - UITableViewDataSource
extension DailyForecastViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: DailyForecastCell.self)) as! DailyForecastCell
        cell.data = data[indexPath.row]
        cell.cellColor = parent?.view.backgroundColor ?? Style.Color.white
        return cell
    }
}
