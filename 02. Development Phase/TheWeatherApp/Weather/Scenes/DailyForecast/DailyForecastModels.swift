//
//  DailyForecastModels.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 09/01/2019.
//  Copyright (c) 2019 Martin Pastorin. All rights reserved.
//

enum DailyForecastModels {
    
    // MARK: Use cases
    
    enum FetchDailyForecast {
        
        struct Request {
            
        }
        
        struct Response {
            var forecast: [DailyForecast]?
        }
        
        struct ViewModel {
            var dailyForecast = [DailyForecastCellViewModel]()
            
            struct Item: DailyForecastCellViewModel {
                let weekday: String
                let minTemperature: String
                let maxTemperature: String
            }
        }
    }
}
