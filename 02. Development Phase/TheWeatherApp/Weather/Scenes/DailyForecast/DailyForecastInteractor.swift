//
//  DailyForecastInteractor.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 09/01/2019.
//  Copyright (c) 2019 Martin Pastorin. All rights reserved.
//

protocol DailyForecastInteractorInterface {
    func fetchDailyForecast(request: DailyForecastModels.FetchDailyForecast.Request)
}

protocol DailyForecastDataStore {
    var forecast: Forecast? { get set }
}

class DailyForecastInteractor: DailyForecastInteractorInterface, DailyForecastDataStore {
   
    // MARK: - Properties
    var presenter: DailyForecastPresenterInterface?
    var forecast: Forecast? {
        didSet {
            let request = DailyForecastModels.FetchDailyForecast.Request()
            fetchDailyForecast(request: request)
        }
    }
    
    // MARK: - DailyForecastInteractorInterface
    func fetchDailyForecast(request: DailyForecastModels.FetchDailyForecast.Request) {
        var response = DailyForecastModels.FetchDailyForecast.Response()
        
        guard let forecast = forecast else {
            presenter?.presentDailyForecast(response: response)
            return
        }
        
        response.forecast = forecast.dailyForecast
        presenter?.presentDailyForecast(response: response)
    }
}
