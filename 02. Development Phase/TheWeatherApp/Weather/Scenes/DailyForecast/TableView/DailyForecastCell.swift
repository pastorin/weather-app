//
//  DailyForecastCell.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 09/01/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import UIKit

protocol DailyForecastCellViewModel {
    var weekday: String { get }
    var minTemperature: String { get }
    var maxTemperature: String { get }
}

class DailyForecastCell: UITableViewCell {
    
    // MARK: - Outlets
    @IBOutlet private(set) weak var weekdayLabel: UILabel!
    @IBOutlet private(set) weak var minTemperatureLabel: UILabel!
    @IBOutlet private(set) weak var maxTemperatureLabel: UILabel!
    
    // MARK: - Properties
    var cellColor = Style.Color.white {
        didSet {
            setupUI()
        }
    }
    var data: DailyForecastCellViewModel? {
        didSet {
            updateDataContent()
        }
    }
    
    // MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    // MARK: - Helpers
    private func setupUI() {
        weekdayLabel.setStyle(.bold(color: cellColor))
        minTemperatureLabel.setStyle(.bold(color: cellColor.withAlphaComponent(Style.Alpha.light)))
        maxTemperatureLabel.setStyle(.bold(color: cellColor))
    }
    
    private func updateDataContent() {
        guard let data = data else {
            return
        }
        
        weekdayLabel.text = data.weekday.uppercased()
        minTemperatureLabel.text = data.minTemperature
        maxTemperatureLabel.text = data.maxTemperature
    }
}
