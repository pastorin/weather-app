//
//  DailyForecastPresenter.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 09/01/2019.
//  Copyright (c) 2019 Martin Pastorin. All rights reserved.
//

import UIKit

protocol DailyForecastPresenterInterface {
    func presentDailyForecast(response: DailyForecastModels.FetchDailyForecast.Response)
}

class DailyForecastPresenter: DailyForecastPresenterInterface {
    
    // MARK: - Properties
    
    weak var viewController: DailyForecastDisplayInterface?
    
    // MARK: - DailyForecastPresenterInterface
    
    func presentDailyForecast(response: DailyForecastModels.FetchDailyForecast.Response) {
        var viewModel = DailyForecastModels.FetchDailyForecast.ViewModel()
        
        guard let forecast = response.forecast else {
            viewController?.displayDailyForecast(viewModel: viewModel)
            return
        }
        
        viewModel.dailyForecast = forecast.compactMap { item -> DailyForecastCellViewModel? in
            guard
                let minTemperature = item.celsiusTemperature.min,
                let maxTemperature = item.celsiusTemperature.max else {
                    return nil
            }
            
            return DailyForecastModels.FetchDailyForecast.ViewModel.Item(weekday: item.date.weekdayReadable(),
                                                                         minTemperature: minTemperature.temperatureReadable(),
                                                                         maxTemperature: maxTemperature.temperatureReadable())
        }
        
        viewController?.displayDailyForecast(viewModel: viewModel)
    }
}
