//
//  DailyForecastRouter.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 09/01/2019.
//  Copyright (c) 2019 Martin Pastorin. All rights reserved.
//

import UIKit

protocol DailyForecastRouterInterface {
    
}

protocol DailyForecastDataPassing {
    var dataStore: DailyForecastDataStore? { get }
}

class DailyForecastRouter: DailyForecastRouterInterface, DailyForecastDataPassing {
    
    // MARK: - Properties
    weak var viewController: DailyForecastViewController?
    var dataStore: DailyForecastDataStore?
}
