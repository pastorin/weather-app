//
//  ForecastOverviewView.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 08/01/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import UIKit

protocol ForecastOverviewViewModel: LocationTitleViewModel {
    var locationName: String { get set }
    var date: String? { get set }
    var iconName: String? { get set }
    var weatherDescription: String? { get set }
    var currentTemperature: String? { get set }
    var feelsLike: String? { get set }
    var minTemperature: String? { get set }
    var maxTemperature: String? { get set }
    var humidity: String? { get set }
    var precipitation: String? { get set }
    var visibility: String? { get set }
    var wind: String? { get set }
}

struct ForecastConditionInfo: ForecastConditionViewModel {
    var title: String
    var value: String?
}

class ForecastOverviewView: XibView {
    
    // MARK: - Outlets
    @IBOutlet private(set) weak var contentView: UIView!
    @IBOutlet private(set) weak var locationTitleView: LocationTitleView!
    
    @IBOutlet private(set) weak var currentTempLabel: UILabel!
    @IBOutlet private(set) weak var descriptionLabel: UILabel!
    
    @IBOutlet private(set) weak var feelsLikeConditionView: ForecastConditionView!
    @IBOutlet private(set) weak var minConditionView: ForecastConditionView!
    @IBOutlet private(set) weak var maxConditionView: ForecastConditionView!
    @IBOutlet private(set) weak var humidityConditionView: ForecastConditionView!
    @IBOutlet private(set) weak var precipitationConditionView: ForecastConditionView!
    @IBOutlet private(set) weak var visibilityConditionView: ForecastConditionView!
    @IBOutlet private(set) weak var windConditionView: ForecastConditionView!
    
    // MARK: - Properties
    var data: ForecastOverviewViewModel? {
        didSet {
            updateDataContent()
        }
    }
    
    override var xibContentView: UIView? {
        return contentView
    }
    
    // MARK: - Lifecycle
    override func layoutSubviews() {
        super.layoutSubviews()
        backgroundColor = .clear
        updateDataContent()
    }
    
    // MARK: - Setup
    override func setupUI() {
        super.setupUI()
        
        descriptionLabel.setStyle(.regular(color: Style.Color.white))
        currentTempLabel.setStyle(.bold(color: Style.Color.white))
    }
    
    // MARK: - Helpers
    private func updateDataContent() {
        locationTitleView.data = data
        
        descriptionLabel.text = data?.weatherDescription ?? LocalizedKeys.Core.unknown
        currentTempLabel.text = data?.currentTemperature ?? LocalizedKeys.Core.emptyValue
        
        feelsLikeConditionView.data = ForecastConditionInfo(title: LocalizedKeys.ForecastOverview.feelsLike, value: data?.feelsLike)
        minConditionView.data = ForecastConditionInfo(title: LocalizedKeys.ForecastOverview.min, value: data?.minTemperature)
        maxConditionView.data = ForecastConditionInfo(title: LocalizedKeys.ForecastOverview.max, value: data?.maxTemperature)
        humidityConditionView.data = ForecastConditionInfo(title: LocalizedKeys.ForecastOverview.humidity, value: data?.humidity)
        precipitationConditionView.data = ForecastConditionInfo(title: LocalizedKeys.ForecastOverview.precipitation, value: data?.precipitation)
        visibilityConditionView.data = ForecastConditionInfo(title: LocalizedKeys.ForecastOverview.visibility, value: data?.visibility)
        windConditionView.data = ForecastConditionInfo(title: LocalizedKeys.ForecastOverview.wind, value: data?.wind)
    }
}
