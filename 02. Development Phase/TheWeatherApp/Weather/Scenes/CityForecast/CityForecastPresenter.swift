//
//  CityForecastPresenter.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 08/01/2019.
//  Copyright (c) 2019 Martin Pastorin. All rights reserved.
//

import UIKit

protocol CityForecastPresenterInterface {
    func presentForecastType(response: CityForecastModels.FetchForecastType.Response)
    func presentForecast(response: CityForecastModels.FetchForecast.Response)
}

class CityForecastPresenter: CityForecastPresenterInterface {
    
    // MARK: - Properties
    weak var viewController: CityForecastDisplayInterface?
    
    // MARK: - CityForecastPresenterInterface
    func presentForecastType(response: CityForecastModels.FetchForecastType.Response) {
        var viewModel = CityForecastModels.FetchForecastType.ViewModel()
        viewModel.forecastOverview = CityForecastModels.ForecastOverviewItem(locationName: response.forecastType.locationName)
        viewController?.displayForecastType(viewModel: viewModel)
    }
    
    func presentForecast(response: CityForecastModels.FetchForecast.Response) {
        var viewModel = CityForecastModels.FetchForecast.ViewModel()
        viewModel.error = response.error
        
        guard let locationName = response.forecastType?.locationName.readableLocationName() else {
            viewController?.displayForecast(viewModel: viewModel)
            return
        }
        
        viewModel.forecastOverview = CityForecastModels.ForecastOverviewItem(locationName: locationName)
        
        guard let forecast = response.forecast else {
            viewController?.displayForecast(viewModel: viewModel)
            return
        }
        
        let temperature = forecast.celsiusTemperature
        
        viewModel.forecast = forecast
        viewModel.forecastOverview?.date = forecast.date.readable()
        viewModel.forecastOverview?.iconName = forecast.iconCode.iconFilename()
        viewModel.forecastOverview?.weatherDescription = forecast.weatherDescription
        viewModel.forecastOverview?.currentTemperature = temperature.current?.temperatureReadable()
        viewModel.forecastOverview?.feelsLike = temperature.feelsLike?.temperatureReadable()
        viewModel.forecastOverview?.minTemperature = temperature.min?.temperatureReadable()
        viewModel.forecastOverview?.maxTemperature = temperature.max?.temperatureReadable()
        viewModel.forecastOverview?.humidity = forecast.humidity?.percentageReadable()
        viewModel.forecastOverview?.precipitation = forecast.precipitation?.precipitationReadable()
        viewModel.forecastOverview?.visibility = forecast.visibility?.kilometreReadable()
        viewModel.forecastOverview?.wind = "\(forecast.wind?.direction ?? "") \(forecast.wind?.speed.kilometrePerHourReadable() ?? "")"
        
        viewController?.displayForecast(viewModel: viewModel)
    }
}
