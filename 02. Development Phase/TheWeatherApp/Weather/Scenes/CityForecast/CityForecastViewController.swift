//
//  CityForecastViewController.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 08/01/2019.
//  Copyright (c) 2019 Martin Pastorin. All rights reserved.
//

import UIKit

protocol CityForecastDisplayInterface: class {
    func displayForecastType(viewModel: CityForecastModels.FetchForecastType.ViewModel)
    func displayForecast(viewModel: CityForecastModels.FetchForecast.ViewModel)
}

class CityForecastViewController: WAViewController {
    
    // MARK: - Outlets
    @IBOutlet private(set) weak var forecastOverviewView: ForecastOverviewView!
    
    // MARK: - Properties
    var interactor: CityForecastInteractorInterface?
    var router: (CityForecastRouterInterface & CityForecastDataPassing)?
    var hourlyForecastViewController: HourlyForecastViewController?
    var dailyForecastViewController: DailyForecastViewController?
    
    // MARK: - Init
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupVIPCycle()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupVIPCycle()
    }
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        performFetchForecastTypeRequest()
        performFetchForecastRequest()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.destination {
        case is HourlyForecastViewController:
            hourlyForecastViewController = segue.destination as? HourlyForecastViewController
        case is DailyForecastViewController:
            dailyForecastViewController = segue.destination as? DailyForecastViewController
        default:
            break
        }
    }
    
    // MARK: - Setup
    private func setupVIPCycle() {
        let viewController = self
        let interactor = CityForecastInteractor()
        let presenter = CityForecastPresenter()
        let router = CityForecastRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }
    
    private func setupUI() {
        view.backgroundColor = Style.Color.green
    }
    
    // MARK: - Helpers
    private func performFetchForecastRequest() {
        let request = CityForecastModels.FetchForecast.Request()
        interactor?.fetchForecast(request: request)
    }
    
    private func performFetchForecastTypeRequest() {
        let request = CityForecastModels.FetchForecastType.Request()
        interactor?.fetchForecastType(request: request)
    }
}

// MARK: - CityForecastDisplayInterface
extension CityForecastViewController: CityForecastDisplayInterface {
    func displayForecastType(viewModel: CityForecastModels.FetchForecastType.ViewModel) {
        forecastOverviewView.data = viewModel.forecastOverview
    }
    
    func displayForecast(viewModel: CityForecastModels.FetchForecast.ViewModel) {
        guard
            let forecast = viewModel.forecast,
            let forecastOverview = viewModel.forecastOverview else {
                return
        }
        
        // Set data for forecastOverviewView
        forecastOverviewView.data = forecastOverview
        
        // Set dataStore for hourlyForecastViewController
        var hourlyForecastDataStore = hourlyForecastViewController?.router?.dataStore
        hourlyForecastDataStore?.forecast = forecast
        
        // Set dataStore for dailyForecastViewController
        var dailyForecastDataStore = dailyForecastViewController?.router?.dataStore
        dailyForecastDataStore?.forecast = forecast
    }
}
