//
//  CityForecastInteractor.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 08/01/2019.
//  Copyright (c) 2019 Martin Pastorin. All rights reserved.
//

import CoreLocation

protocol CityForecastInteractorInterface {
    func fetchForecastType(request: CityForecastModels.FetchForecastType.Request)
    func fetchForecast(request: CityForecastModels.FetchForecast.Request)
}

protocol CityForecastDataStore {
    var forecastType: ForecastType? { get set }
}

class CityForecastInteractor: CityForecastInteractorInterface, CityForecastDataStore {
   
    // MARK: - Properties
    var presenter: CityForecastPresenterInterface?
    var worker: CityForecastWorkerInterface = CityForecastWorker()
    var forecastType: ForecastType?
    
    // MARK: - CityForecastInteractorInterface
    func fetchForecastType(request: CityForecastModels.FetchForecastType.Request) {
        guard let forecastType = forecastType else {
            return
        }
        
        let response = CityForecastModels.FetchForecastType.Response(forecastType: forecastType)
        presenter?.presentForecastType(response: response)
    }
    
    func fetchForecast(request: CityForecastModels.FetchForecast.Request) {
        guard let forecastType = forecastType else {
            return
        }
        
        var response = CityForecastModels.FetchForecast.Response()
        response.forecastType = forecastType
        
        worker.fetchForecast(for: forecastType) { [weak self] result in
            switch result {
            case .success(let forecast):
                response.forecast = forecast
            case .failure(let error):
                response.error = error
            }

            self?.presenter?.presentForecast(response: response)
        }
    }
}
