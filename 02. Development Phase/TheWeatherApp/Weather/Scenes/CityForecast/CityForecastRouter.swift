//
//  CityForecastRouter.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 08/01/2019.
//  Copyright (c) 2019 Martin Pastorin. All rights reserved.
//

import UIKit

protocol CityForecastRouterInterface {
    
}

protocol CityForecastDataPassing {
    var dataStore: CityForecastDataStore? { get }
}

class CityForecastRouter: CityForecastRouterInterface, CityForecastDataPassing {
    
    // MARK: - Properties
    weak var viewController: CityForecastViewController?
    var dataStore: CityForecastDataStore?
}
