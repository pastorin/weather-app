//
//  CityForecastModels.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 08/01/2019.
//  Copyright (c) 2019 Martin Pastorin. All rights reserved.
//

enum CityForecastModels {
    
    struct ForecastOverviewItem: ForecastOverviewViewModel {
        var locationName: String
        var date: String?
        var iconName: String?
        var weatherDescription: String?
        var currentTemperature: String?
        var feelsLike: String?
        var minTemperature: String?
        var maxTemperature: String?
        var humidity: String?
        var precipitation: String?
        var visibility: String?
        var wind: String?
    }
    
    // MARK: Use cases
    enum FetchForecastType {
        struct Request {
            
        }
        
        struct Response {
            var forecastType: ForecastType
        }
        
        struct ViewModel {
            var forecastOverview: ForecastOverviewViewModel?
        }
    }
    
    enum FetchForecast {
        struct Request {
            
        }
        
        struct Response {
            var forecastType: ForecastType?
            var forecast: Forecast?
            var error: Error?
        }
        
        struct ViewModel {
            var forecast: Forecast?
            var forecastOverview: ForecastOverviewViewModel?
            var error: Error?
            var errorMessage: String? {
                return error?.localizedDescription
            }
        }
    }

}

extension CityForecastModels.ForecastOverviewItem {
    init(locationName: String) {
        self.locationName = locationName
    }
}
