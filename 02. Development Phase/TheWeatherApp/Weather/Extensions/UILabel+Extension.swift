//
//  UILabel+Extension.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 08/01/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import UIKit

extension UILabel {
    enum LabelStyle {
        case bold(color: UIColor)
        case regular(color: UIColor)
        case light(color: UIColor)
        case medium(color: UIColor)
    }
    
    func setStyle(_ style: LabelStyle) {
        switch style {
        case .bold(let color):
            textColor = color
            font = Style.Font.Bold(size: font.pointSize)
        case .regular(let color):
            textColor = color
            font = Style.Font.Regular(size: font.pointSize)
        case .light(let color):
            textColor = color
            font = Style.Font.Light(size: font.pointSize)
        case .medium(let color):
            textColor = color
            font = Style.Font.Medium(size: font.pointSize)
        }
    }
}
