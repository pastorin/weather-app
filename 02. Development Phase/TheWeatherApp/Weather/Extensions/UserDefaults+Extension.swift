//
//  UserDefaults+Extension.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 16/01/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import Foundation

enum UserDefaultsKeys: String, CaseIterable {
    case locationNames
}

extension UserDefaults {
    
    var locationNames: [String] {
        set {
            set(newValue, forKey: UserDefaultsKeys.locationNames.rawValue)
        }
        get {
            return stringArray(forKey: UserDefaultsKeys.locationNames.rawValue) ?? []
        }
    }
    
}
