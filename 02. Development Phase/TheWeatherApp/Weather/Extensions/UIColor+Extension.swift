//
//  UIColor+Extension.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 08/01/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import UIKit

extension UIColor {
    
    convenience init(hex: UInt32, alpha: CGFloat = 1) {
        let divisor = CGFloat(255)
        let red = CGFloat((hex >> 16) & 0xFF) / divisor
        let green = CGFloat((hex >> 8) & 0xFF) / divisor
        let blue = CGFloat(hex & 0xFF) / divisor
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }
    
}
