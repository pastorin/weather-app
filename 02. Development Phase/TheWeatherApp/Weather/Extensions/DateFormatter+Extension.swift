//
//  DateFormatter+Extension.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 08/01/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import Foundation

extension DateFormatter {
    
    /**
     Used in the Networking layer
     */
    static let timelessDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    
    static let apiDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm"
        
        if let timeZone = NSTimeZone(abbreviation: "UTC") as TimeZone? {
            formatter.timeZone = timeZone
        }
        
        return formatter
    }()
    
    /**
     Provides a dateFormatter that can parse a Date object into a readable format
     E.g.: Monday, %@ May
     */
    static let readableDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "EEEE, %@ MMM"
        return formatter
    }()
    
    /**
     Provides a dateFormatter that can parse a Date object into a time readable format
     E.g.: 5 pm
     */
    static let readableTimeFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "h a"
        
        if let timeZone = NSTimeZone(abbreviation: "UTC") as TimeZone? {
            formatter.timeZone = timeZone
        }
        
        return formatter
    }()
    
    /**
     Provides a dateFormatter that can parse a Date object into a weekday readable format
     E.g.: Wednesday 14
     */
    static let readableWeekdayFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "EEEE d"
        return formatter
    }()
}
