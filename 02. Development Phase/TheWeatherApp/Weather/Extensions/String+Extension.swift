//
//  String+Extension.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 08/01/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import Foundation

extension String {
    
    func timelessDate() -> Date? {
        let formatter = DateFormatter.timelessDateFormatter
        return formatter.date(from: self)
    }
    
    func date() -> Date? {
        let formatter = DateFormatter.apiDateFormatter
        return formatter.date(from: self)
    }
    
    func readableLocationName() -> String {
        return components(separatedBy: ",").first ?? self
    }
    
    func iconFilename() -> String {
        return "icon_\(self)"
    }
    
}
