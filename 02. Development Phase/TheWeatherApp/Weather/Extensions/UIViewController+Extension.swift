//
//  UIViewController+Utils.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 08/01/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import UIKit

extension UIViewController {
    static func from(storyboard: Storyboards) -> UIViewController? {
        let storyboard = UIStoryboard(name: storyboard.rawValue, bundle: nil)
        return storyboard.instantiateInitialViewController()
    }
}
