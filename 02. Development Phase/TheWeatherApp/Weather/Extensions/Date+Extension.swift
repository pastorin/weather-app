//
//  Date+Extension.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 08/01/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import Foundation

extension Date {
    
    func readable() -> String? {
        let dateComponents = Calendar.current.component(.day, from: self)
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .ordinal
        
        guard let day = numberFormatter.string(from: dateComponents as NSNumber) else {
            return nil
        }
        
        return String(format: DateFormatter.readableDateFormatter.string(from: self), day)
    }
    
    func timeReadable() -> String {
        return DateFormatter.readableTimeFormatter.string(from: self)
    }
    
    func weekdayReadable() -> String {
        return DateFormatter.readableWeekdayFormatter.string(from: self)
    }
}
