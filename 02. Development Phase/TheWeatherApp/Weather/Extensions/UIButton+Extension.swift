//
//  UIButton+Extension.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 09/01/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import UIKit

extension UIButton {
    enum ButtonStyle {
        case green
    }
    
    func setStyle(_ style: ButtonStyle) {
        layer.cornerRadius = Style.CornerRadius.regular
        
        switch style {
        case .green:
            titleLabel?.font = Style.Font.Bold(size: titleLabel?.font.pointSize ?? Style.Font.FontSize.regular)
            backgroundColor = isEnabled ? Style.Color.green : Style.Color.gray
            setTitleColor(Style.Color.white, for: .normal)
        }
    }
    
    func setTitle(_ title: String?, for state: UIControl.State, animated: Bool) {
        if animated {
            setTitle(title?.uppercased(), for: state)
        } else {
            UIView.setAnimationsEnabled(false)
            setTitle(title?.uppercased(), for: state)
            layoutIfNeeded()
            UIView.setAnimationsEnabled(true)
        }
    }

}
