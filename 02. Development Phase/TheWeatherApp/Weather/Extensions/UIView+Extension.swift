//
//  UIView+Extension.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 20/01/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import UIKit

extension UIView {
    
    class func fromNib<T: UIView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)!.first as! T
    }
}
