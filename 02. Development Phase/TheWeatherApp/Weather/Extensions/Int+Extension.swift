//
//  Int+Extension.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 08/01/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import Foundation

extension Int {
    
    func temperatureReadable() -> String {
        return String(format: "%d", self)
    }
    
    func percentageReadable() -> String {
        return String(format: "%d%%", self)
    }
    
    func kilometreReadable() -> String {
        return String(format: "%d km", self)
    }
    
    func kilometrePerHourReadable() -> String {
        return String(format: "%dkm/h", self)
    }
    
}
