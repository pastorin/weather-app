//
//  Float+Extension.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 08/01/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import Foundation

extension Float {

    func precipitationReadable() -> String {
        return String(format: "%.1f mm", self)
    }
}
