//
//  Forecast.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 08/01/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import Foundation
import Alamofire

struct Forecast {
    let type: APIForecastRequestType
    let locationName: String
    let date: Date
    let weatherDescription: String?
    let iconCode: String

    let humidity: Int?
    let precipitation: Float?
    let visibility: Int?

    let wind: Wind?
    let celsiusTemperature: Temperature
    let fahrenheitTemperature: Temperature

    let hourlyForecast: [HourlyForecast]
    let dailyForecast: [DailyForecast]
}

extension Forecast: Decodable {
    
    enum CodingKeys: String, CodingKey {
        case data, request
        case timeZone = "time_zone"
        case forecast = "weather"
        case currentCondition = "current_condition"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let data = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .data)
        let requestData = try data.decode([APIForecastRequest].self, forKey: .request)
        let forecastConditionData = try data.decode([APIForecastCondition].self, forKey: .currentCondition)
        let forecastData = try data.decode([APIWeather].self, forKey: .forecast)
        let timeZoneData = try data.decode([APITimeZone].self, forKey: .timeZone)
        
        guard
            let request = requestData.first,
            let forecastCondition = forecastConditionData.first,
            let forecast = forecastData.first,
            let forecastDate = timeZoneData.first?.localtime.date() else {
                throw AFError.responseSerializationFailed(reason: .inputDataNil)
        }
        
        type = request.type
        locationName = request.info
        date = forecastDate
        weatherDescription = forecastCondition.weatherDesc.first?.value
        iconCode = forecastCondition.weatherCode
        humidity = Int(forecastCondition.humidity)
        precipitation = Float(forecastCondition.precipitation)
        visibility = Int(forecastCondition.visibility)

        wind = Wind(with: forecastCondition)

        celsiusTemperature = Temperature(current: forecastCondition.temperatureC,
                                         feelsLike: forecastCondition.feelsLikeC,
                                         min: forecast.mintempC,
                                         max: forecast.maxtempC)
        
        fahrenheitTemperature = Temperature(current: forecastCondition.temperatureF,
                                            feelsLike: forecastCondition.feelsLikeF,
                                            min: forecast.mintempF,
                                            max: forecast.maxtempF)

        hourlyForecast = forecast.hourly.compactMap { HourlyForecast(with: $0, currentDate: forecastDate) }
        dailyForecast = forecastData.dropFirst().compactMap { DailyForecast(with: $0) }
        
    }
}
