//
//  HourlyForecast.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 08/01/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import Foundation

struct HourlyForecast {
    let date: Date
    let iconCode: String
    let celsiusTemperature: Temperature
    let fahrenheitTemperature: Temperature
}

extension HourlyForecast {
    
    init?(with data: APIWeather.Hourly, currentDate: Date) {
        guard let time = Double(data.time) else {
            return nil
        }
        date = currentDate.addingTimeInterval(time * 0.6 * 60)
        iconCode = data.weatherCode
        celsiusTemperature = Temperature(current: data.tempC)
        fahrenheitTemperature = Temperature(current: data.tempF)
    }
}
