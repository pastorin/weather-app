//
//  WeatherForecast.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 12/01/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import Foundation

struct WeatherForecast {
    let id = UUID().uuidString
    var forecastType: ForecastType
    var forecast: Forecast?
}

extension WeatherForecast: Equatable {
    static func == (lhs: WeatherForecast, rhs: WeatherForecast) -> Bool {
        return lhs.forecastType == rhs.forecastType
    }
}
