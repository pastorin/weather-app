//
//  SearchResults.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 18/01/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

struct SearchResult {
    let locations: [APILocation]
}

extension SearchResult: Decodable {
    
    enum CodingKeys: String, CodingKey {
        case result
        case data = "search_api"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let data = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .data)
        locations = try data.decode([APILocation].self, forKey: .result)
    }
}
