//
//  APIWeather.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 08/01/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

struct APIWeather {
    struct Hourly: Decodable {
        let weatherCode: String
        let time: String
        let tempC: String
        let tempF: String
    }
    
    let date: String
    let maxtempC: String
    let maxtempF: String
    let mintempC: String
    let mintempF: String
    let hourly: [APIWeather.Hourly]
}

extension APIWeather: Decodable { }
