//
//  APILocation.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 18/01/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import Alamofire

struct APILocation {
    struct Data: Decodable {
        let value: String
    }
    
    let areaName: String
    let country: String
    let region: String
}

extension APILocation: Decodable {
    enum CodingKeys: String, CodingKey {
        case areaName, country, region
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let areaData = try container.decode([Data].self, forKey: .areaName)
        let countryData = try container.decode([Data].self, forKey: .country)
        let regionData = try container.decode([Data].self, forKey: .region)
        
        guard
            let areaNameValue = areaData.first?.value,
            let countryValue = countryData.first?.value,
            let regionValue = regionData.first?.value else {
                throw AFError.responseSerializationFailed(reason: .inputDataNil)
        }
        
        areaName = areaNameValue
        country = countryValue
        region = regionValue
    }
}
