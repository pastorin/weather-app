//
//  APIError.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 08/01/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import Foundation

struct APIError: LocalizedError {
    var errorDescription: String?
}

extension APIError: Decodable {
    private struct Messages: Codable {
        let msg: String
    }
    
    private enum CodingKeys: String, CodingKey {
        case data, error
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let data = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .data)
        let errorMessages = try data.decode([Messages].self, forKey: .error)
        errorDescription = errorMessages.first?.msg
    }
}
