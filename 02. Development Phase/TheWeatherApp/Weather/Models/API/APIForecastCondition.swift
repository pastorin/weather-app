//
//  APIForecastCondition.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 08/01/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

struct APIForecastCondition {
    struct Description: Decodable {
        let value: String
    }
    
    let time: String
    let weatherCode: String
    let weatherDesc: [Description]
    let temperatureC: String
    let temperatureF: String
    let feelsLikeC: String
    let feelsLikeF: String
    let humidity: String
    let visibility: String
    let precipitation: String
    let windSpeed: String
    let windDegree: String
    let windDirection: String
}

extension APIForecastCondition: Decodable {
    
    enum CodingKeys: String, CodingKey {
        case weatherCode, weatherDesc, humidity, visibility
        case precipitation = "precipMM"
        case time = "observation_time"
        case temperatureC = "temp_C"
        case temperatureF = "temp_F"
        case feelsLikeC = "FeelsLikeC"
        case feelsLikeF = "FeelsLikeF"
        case windSpeed = "windspeedKmph"
        case windDegree = "winddirDegree"
        case windDirection = "winddir16Point"
    }
}
