//
//  APIForecastRequest.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 08/01/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import Alamofire

struct APIForecastRequest {
    let type: APIForecastRequestType
    let info: String
}

extension APIForecastRequest: Decodable {
    enum CodingKeys: String, CodingKey {
        case type
        case info = "query"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        guard let requestType = APIForecastRequestType(rawValue: try container.decode(String.self, forKey: .type)) else {
            throw AFError.responseSerializationFailed(reason: .inputDataNil)
        }
        type = requestType
        info = try container.decode(String.self, forKey: .info)
    }
}
