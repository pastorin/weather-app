//
//  APITimeZone.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 15/01/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

struct APITimeZone {
    let localtime: String
    let utcOffset: String
    let zone: String
}

extension APITimeZone: Decodable { }
