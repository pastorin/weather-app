//
//  APIForecastRequestType.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 08/01/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

enum APIForecastRequestType: String {
    case city = "City"
    case location = "LatLon"
}
