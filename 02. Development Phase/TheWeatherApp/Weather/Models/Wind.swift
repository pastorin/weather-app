//
//  Wind.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 08/01/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import Foundation

struct Wind {
    let direction: String
    let degree: String
    let speed: Int
}

extension Wind {
    init?(with data: APIForecastCondition) {
        guard let speedValue = Int(data.windSpeed) else {
            return nil
        }
        direction = data.windDirection
        degree = data.windDegree
        speed = speedValue
    }
}
