//
//  ForecastType.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 09/01/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import CoreLocation

enum ForecastType {
    case locationCoordinate(coordinate: CLLocationCoordinate2D?)
    case locationName(name: String)
}

extension ForecastType: Equatable { }
extension ForecastType {
    var locationName: String {
        switch self {
        case .locationName(let name):
            return name
        default:
            return LocalizedKeys.Core.currentLocation
        }
    }
}

extension CLLocationCoordinate2D: Equatable {
    public static func == (lhs: CLLocationCoordinate2D, rhs: CLLocationCoordinate2D) -> Bool {
        return lhs.latitude == rhs.latitude && lhs.longitude == rhs.longitude
    }
}
