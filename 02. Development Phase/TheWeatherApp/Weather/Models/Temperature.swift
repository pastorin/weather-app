//
//  Temperature.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 08/01/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import Foundation

enum TemperatureScale {
    case celsius
    case fahrenheit
}

struct Temperature {
    let current: Int?
    let feelsLike: Int?
    let min: Int?
    let max: Int?
}

extension Temperature {
    init(current: String = "", feelsLike: String = "", min: String = "", max: String = "") {
        self.current = Int(current)
        self.feelsLike = Int(feelsLike)
        self.min = Int(min)
        self.max = Int(max)
    }
}
