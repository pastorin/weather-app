//
//  DailyForecast.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 08/01/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import Foundation

struct DailyForecast {
    let date: Date
    let celsiusTemperature: Temperature
    let fahrenheitTemperature: Temperature
}

extension DailyForecast {
    
    init?(with data: APIWeather) {
        guard let timelessDate = data.date.timelessDate() else {
            return nil
        }
        date = timelessDate
        celsiusTemperature = Temperature(min: data.mintempC, max: data.maxtempC)
        fahrenheitTemperature = Temperature(min: data.mintempF, max: data.maxtempF)
    }
}
