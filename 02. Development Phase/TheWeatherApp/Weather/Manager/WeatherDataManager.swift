//
//  WeatherDataManager.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 14/01/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import Foundation
import CoreLocation

protocol WeatherDataManagerObserver: class {
    func weatherDataManager(_ manager: WeatherDataManager, didChangeLocation coordinate: CLLocationCoordinate2D?)
    func weatherDataManager(_ manager: WeatherDataManager, didAddLocation location: String)
    func weatherDataManager(_ manager: WeatherDataManager, didRemoveLocation location: String)
}

extension WeatherDataManagerObserver {
    func weatherDataManager(_ manager: WeatherDataManager, didChangeLocation coordinate: CLLocationCoordinate2D?) { }
    func weatherDataManager(_ manager: WeatherDataManager, didAddLocation location: String) { }
    func weatherDataManager(_ manager: WeatherDataManager, didRemoveLocation location: String) { }
}

class WeatherDataManager: NSObject {
    
    // MARK: - Singleton
    static let sharedManager = WeatherDataManager()
    
    // MARK: - Properties
    private var locationManager = CLLocationManager()
    private var observations = [ObjectIdentifier: Observation]()
    
    var allLocations = UserDefaults.standard.locationNames {
        didSet {
            UserDefaults.standard.locationNames = allLocations
        }
    }
    var allWeatherForecast = [WeatherForecast]()
    var allForecastType: [ForecastType] {
        return allWeatherForecast.map { $0.forecastType }
    }
    
    // MARK: - Init
    override init() {
        super.init()
        createForecastsList()
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers
        locationManager.startUpdatingLocation()
    }
    
    // MARK: - Public
    func addLocation(_ location: String) {
        guard !allLocations.contains(where: { $0 == location }) else {
            return
        }
        
        let weatherForecast = WeatherForecast(forecastType: .locationName(name: location), forecast: nil)
        allLocations.append(location)
        allWeatherForecast.append(weatherForecast)
        
        notifiyObservers(for: .didAddLocation(location: location))
    }
    
    func removeLocation(_ location: String) {
        allLocations.removeAll { $0 == location }
        allWeatherForecast.removeAll {
            if case let .locationName(locationName) = $0.forecastType, locationName == location {
                return true
            }
            return false
        }
        
        notifiyObservers(for: .didRemoveLocation(location: location))
    }
    
    func updateForecast(_ forecast: Forecast?, for forecastType: ForecastType) {
        guard let index = allWeatherForecast.firstIndex(where: { $0.forecastType == forecastType }) else {
            return
        }
        
        var weatherForecast = allWeatherForecast[index]
        weatherForecast.forecast = forecast
        allWeatherForecast[index] = weatherForecast
    }
    
    func forecast(for forecastType: ForecastType) -> Forecast? {
        let weatherForecast = allWeatherForecast.first { $0.forecastType == forecastType }
        return weatherForecast?.forecast
    }
    
    // MARK: - Helpers
    private func createForecastsList() {
        // Add current location forecast
        let currentWeatherForecast = WeatherForecast(forecastType: .locationCoordinate(coordinate: locationManager.location?.coordinate), forecast: nil)
        allWeatherForecast.append(currentWeatherForecast)
        
        // Add other forecasts
        allLocations.forEach {
            let weatherForecast = WeatherForecast(forecastType: .locationName(name: $0), forecast: nil)
            allWeatherForecast.append(weatherForecast)
        }
    }
    
    fileprivate func updateCurrentLocation(with coordinate: CLLocationCoordinate2D?) {
        guard
            let locationWeatherForecast = allWeatherForecast.first,
            case let .locationCoordinate(currentCoordinate) = locationWeatherForecast.forecastType,
            currentCoordinate != coordinate else {
                return
        }
        
        allWeatherForecast[0] =  WeatherForecast(forecastType: .locationCoordinate(coordinate: coordinate), forecast: nil)
        notifiyObservers(for: .didChangeLocation(coordinate: coordinate))
    }
}

// MARK: - CLLocationManagerDelegate
extension WeatherDataManager: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else {
            return
        }

        locationManager.stopUpdatingLocation()
        updateCurrentLocation(with: location.coordinate)
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status != .authorizedWhenInUse {
            updateCurrentLocation(with: nil)
        }

        locationManager.startUpdatingLocation()
    }
}

// MARK: - Observation
extension WeatherDataManager {
    fileprivate struct Observation {
        weak var observer: WeatherDataManagerObserver?
    }
    
    fileprivate enum ObservationAction {
        case didChangeLocation(coordinate: CLLocationCoordinate2D?)
        case didAddLocation(location: String)
        case didRemoveLocation(location: String)
    }
    
    // MARK: - Public
    func addObserver(_ observer: WeatherDataManagerObserver) {
        let id = ObjectIdentifier(observer)
        observations[id] = Observation(observer: observer)
    }
    
    func removeObserver(_ observer: WeatherDataManagerObserver) {
        let id = ObjectIdentifier(observer)
        observations.removeValue(forKey: id)
    }
    
    // MARK: - Helpers
    fileprivate func notifiyObservers(for action: ObservationAction) {
        for (id, observation) in observations {
            guard let observer = observation.observer else {
                observations.removeValue(forKey: id)
                continue
            }
            
            switch action {
            case .didChangeLocation(let coordinate):
                observer.weatherDataManager(self, didChangeLocation: coordinate)
            case .didAddLocation(let location):
                observer.weatherDataManager(self, didAddLocation: location)
            case .didRemoveLocation(let location):
                observer.weatherDataManager(self, didRemoveLocation: location)
            }
        }
    }
}
