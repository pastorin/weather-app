//
//  APIConstants.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 08/01/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import Foundation

enum APIConstants {
    
    enum Server {
        static let baseURL: String = {
            let apiURL = Environment.getEnvironmentVariable(name: Environment.VariableNames.apiURL)
            return apiURL + APIConstants.Server.version
        }()
        
        static let clientKey = Environment.getEnvironmentVariable(name: Environment.VariableNames.apiKey)
        static let version = "premium/v1" // "free/v2"
    }
    
    enum ParameterKey {
        static let location = "q"
        static let days = "num_of_days"
        static let timeInterval = "tp"
        static let apiKey = "key"
        static let query = "query"
        static let results = "num_of_results"
        static let format = "format"
        static let monthlyClimateAverage = "mca"
        static let localTime = "showlocaltime"
        static let popularCities = "popular"
    }
    
    enum ParameterValue {
        static let days = 6
        static let timeIntervalHours = 1
        static let results = 20
        static let jsonFormat = "json"
        static let monthlyClimateAverage = "no"
        static let showLocalTime = "yes"
        static let showOnlyPopularCities = "yes"
    }
    
    enum Paths: String {
        case forecast = "weather.ashx"
        case search = "search.ashx"
    }
}

enum HTTPHeaderField: String {
    case contentType = "Content-Type"
    case acceptType = "Accept"
}

enum ContentType: String {
    case json = "application/json"
}
