//
//  APIRequest.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 08/01/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import Foundation
import Alamofire

typealias APIHTTPMethod = HTTPMethod
typealias APIParameters = Parameters
typealias APIResult = Result

protocol APIRequest: URLRequestConvertible {
    var method: APIHTTPMethod { get }
    var path: String { get }
    var parameters: APIParameters? { get }
    var query: APIParameters { get }
}

extension APIRequest {
    var parameters: APIParameters? {
        return nil
    }
    
    var query: APIParameters {
        return [:]
    }
}

extension APIRequest {
    
    private var requestQuery: APIParameters {
        var requestQuery = query
        requestQuery[APIConstants.ParameterKey.apiKey] = APIConstants.Server.clientKey
        requestQuery[APIConstants.ParameterKey.format] = APIConstants.ParameterValue.jsonFormat
        return requestQuery
    }
    
    func asURLRequest() throws -> URLRequest {
        
        let url = try APIConstants.Server.baseURL.asURL()
        let urlRequest = URLRequest(url: url.appendingPathComponent(path))
        var encodedUrlRequest = try URLEncoding.queryString.encode(urlRequest, with: requestQuery)
        
        // HTTP Method
        encodedUrlRequest.httpMethod = method.rawValue
        
        // Common Headers
        encodedUrlRequest.setValue(ContentType.json.rawValue, forHTTPHeaderField: HTTPHeaderField.acceptType.rawValue)
        encodedUrlRequest.setValue(ContentType.json.rawValue, forHTTPHeaderField: HTTPHeaderField.contentType.rawValue)
        
        // Setting parameters to body if it is needed
        if (method == .post || method == .put), let parameters = parameters {
            do {
                encodedUrlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
            } catch {
                throw AFError.parameterEncodingFailed(reason: .jsonEncodingFailed(error: error))
            }
        }
        
        return encodedUrlRequest
    }
}
