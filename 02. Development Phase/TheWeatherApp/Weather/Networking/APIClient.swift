//
//  APIClient.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 08/01/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import Foundation
import Alamofire
import CodableAlamofire

class APIClient {
    
    @discardableResult
    static func perform<T: Decodable>(_ request: APIRequest, decoder: JSONDecoder = JSONDecoder(), completion: @escaping (Result<T>) -> Void) -> DataRequest {
        APIClient.logRequest(request)
        return SessionManager.default.request(request).validate().responseDecodableObject(decoder: decoder) { (response: DataResponse<T>) in
            switch response.result {
            case .success:
                APIClient.logSuccess(for: request)
            case .failure(let error):
                if let data = response.data, let apiError = try? decoder.decode(APIError.self, from: data) {
                    APIClient.logError(error: apiError, for: request)
                    completion(.failure(apiError))
                    return
                }
                APIClient.logError(error: error, for: request)
            }
            completion(response.result)
        }
    }
    
    // MARK: - Helpers
    private static func logRequest(_ request: APIRequest) {
        log.info("\(request.method.rawValue.uppercased()) \"\(request.urlRequest!)\"")
    }
    
    private static func logSuccess(for request: APIRequest) {
        log.verbose("\(request.method.rawValue.uppercased()) \"\(request.urlRequest!)\"")
    }
    
    private static func logError(error: Error, for request: APIRequest) {
        let requestLog = "\(request.method.rawValue.uppercased()) \"\(request.urlRequest!)\""
        log.error(requestLog, context: error.localizedDescription)
    }
}
