//
//  SearchService.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 18/01/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import Alamofire

struct SearchService {
    
    private enum request: APIRequest {
        case locationName(name: String)
        
        var method: APIHTTPMethod {
            return .get
        }
        
        var path: String {
            return APIConstants.Paths.search.rawValue
        }
        
        var query: APIParameters {
            switch self {
            case .locationName(let name):
                return [APIConstants.ParameterKey.query: name.replacingOccurrences(of: " ", with: "+"),
                        APIConstants.ParameterKey.popularCities: APIConstants.ParameterValue.showOnlyPopularCities,
                        APIConstants.ParameterKey.results: APIConstants.ParameterValue.results]
            }
        }
    }
    
    // MARK: - Calls
    @discardableResult
    static func search(for locationName: String, completion: @escaping (APIResult<SearchResult>) -> Void) -> DataRequest {
        let request = SearchService.request.locationName(name: locationName)
        return APIClient.perform(request, completion: completion)
    }
}
