//
//  ForecastService.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 08/01/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import Foundation
import CoreLocation

struct ForecastService {
    
    private enum request: APIRequest {
        case locationCoordinate(latitude: CLLocationDegrees, longitude: CLLocationDegrees)
        case locationName(name: String)
        
        var method: APIHTTPMethod {
            return .get
        }
        
        var path: String {
            return APIConstants.Paths.forecast.rawValue
        }
        
        var query: APIParameters {
            var parameters: APIParameters = [
                APIConstants.ParameterKey.days: APIConstants.ParameterValue.days,
                APIConstants.ParameterKey.timeInterval: APIConstants.ParameterValue.timeIntervalHours,
                APIConstants.ParameterKey.monthlyClimateAverage: APIConstants.ParameterValue.monthlyClimateAverage,
                APIConstants.ParameterKey.localTime: APIConstants.ParameterValue.showLocalTime
            ]
            
            switch self {
            case let .locationCoordinate(latitude, longitude):
                parameters[APIConstants.ParameterKey.location] = String(format: "%f,%f", latitude, longitude)
            case .locationName(let name):
                parameters[APIConstants.ParameterKey.location] = name.replacingOccurrences(of: " ", with: "+")
            }
            
            return parameters
        }
    }
    
    // MARK: - Calls
    static func forecast(for locationCoordinate: CLLocationCoordinate2D, completion: @escaping (APIResult<Forecast>) -> Void) {
        let request = ForecastService.request.locationCoordinate(latitude: locationCoordinate.latitude, longitude: locationCoordinate.longitude)
        APIClient.perform(request, completion: completion)
    }
    
    static func forecast(for locationName: String, completion: @escaping (APIResult<Forecast>) -> Void) {
        let request = ForecastService.request.locationName(name: locationName)
        APIClient.perform(request, completion: completion)
    }
    
}
