//
//  Style.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 08/01/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import UIKit

enum Style {
    enum Font {
        enum FontName {
            static let customFontRegular = "Roboto-Regular"
            static let customFontBold = "Roboto-Bold"
            static let customFontMedium = "Roboto-Medium"
            static let customFontLight = "Roboto-Light"
        }
        
        enum FontSize {
            static let small: CGFloat = 12.0
            static let regular: CGFloat = 14.0
            static let large: CGFloat = 20.0
            static let xlarge: CGFloat = 24.0
            static let xxlarge: CGFloat = 160.0
        }
        
        static func Light(size: CGFloat) -> UIFont {
            return UIFont(name: FontName.customFontLight, size: size)!
        }
        
        static func Regular(size: CGFloat) -> UIFont {
            return UIFont(name: FontName.customFontRegular, size: size)!
        }
        
        static func Bold(size: CGFloat) -> UIFont {
            return UIFont(name: FontName.customFontBold, size: size)!
        }
        
        static func Medium(size: CGFloat) -> UIFont {
            return UIFont(name: FontName.customFontMedium, size: size)!
        }
    }
    
    enum Color {
        static let white = UIColor(hex: 0xFFFFFF)
        static let green = UIColor(hex: 0x12918e)
        static let violet = UIColor(hex: 0x915e96)
        static let pink = UIColor(hex: 0xe55a71)
        static let blue = UIColor(hex: 0x3a91d6)
        static let gray = UIColor(hex: 0x5d5d5d)
    }
    
    enum Alpha {
        static let light: CGFloat = 0.6
        static let medium: CGFloat = 0.3
        static let heavy: CGFloat = 0.2
    }
    
    enum CornerRadius {
        static let regular: CGFloat = 5.0
    }
}
