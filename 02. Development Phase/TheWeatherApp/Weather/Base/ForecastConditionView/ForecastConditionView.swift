//
//  ForecastConditionView.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 08/01/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import UIKit

protocol ForecastConditionViewModel {
    var title: String { get }
    var value: String? { get }
}

class ForecastConditionView: XibView {
    
    // MARK: - Outlets
    @IBOutlet private(set) weak var contentView: UIView!
    @IBOutlet private(set) weak var titleLabel: UILabel!
    @IBOutlet private(set) weak var valueLabel: UILabel!
    
    // MARK: - Properties
    var data: ForecastConditionViewModel? {
        didSet {
            updateDataContent()
        }
    }
    
    override var xibContentView: UIView? {
        return contentView
    }
    
    // MARK: - Lifecycle
    override func layoutSubviews() {
        super.layoutSubviews()
        backgroundColor = .clear
    }
    
    // MARK: - Setup
    override func setupUI() {
        super.setupUI()
        
        titleLabel.setStyle(.regular(color: Style.Color.white))
        valueLabel.setStyle(.bold(color: Style.Color.white))
    }
    
    // MARK: - Helpers
    private func updateDataContent() {
        titleLabel.text = data?.title.capitalized ?? ""
        valueLabel.text = (data?.value ?? LocalizedKeys.Core.emptyValue)
    }
}
