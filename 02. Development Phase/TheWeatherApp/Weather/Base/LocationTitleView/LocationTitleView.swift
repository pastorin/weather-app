//
//  LocationTitleView.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 08/01/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import UIKit

protocol LocationTitleViewModel {
    var locationName: String { get }
    var date: String? { get }
    var iconName: String? { get }
}

class LocationTitleView: XibView {

    // MARK: - Outlets
    @IBOutlet private(set) weak var contentView: UIView!
    @IBOutlet private(set) weak var iconImageView: UIImageView!
    @IBOutlet private(set) weak var locationNameLabel: UILabel!
    @IBOutlet private(set) weak var dateLabel: UILabel!
    
    // MARK: - Properties
    var data: LocationTitleViewModel? {
        didSet {
            updateDataContent()
        }
    }
    
    override var xibContentView: UIView? {
        return contentView
    }
    
    // MARK: - Lifecycle
    override func layoutSubviews() {
        super.layoutSubviews()
        backgroundColor = .clear
    }
    
    // MARK: - Setup
    override func setupUI() {
        super.setupUI()
        
        locationNameLabel.setStyle(.bold(color: Style.Color.white))
        dateLabel.setStyle(.regular(color: Style.Color.white))
    }
    
    // MARK: - Helpers
    private func updateDataContent() {
        locationNameLabel.text = data?.locationName.uppercased() ?? LocalizedKeys.Core.unknown
        dateLabel.text = data?.date?.uppercased() ?? ""
        iconImageView.image = UIImage(named: data?.iconName ?? LocalizedKeys.Core.defaultIconName)
    }
}
