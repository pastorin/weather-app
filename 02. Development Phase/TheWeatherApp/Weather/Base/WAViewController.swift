//
//  WAViewController.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 15/01/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import UIKit

class WAViewController: UIViewController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
