//
//  XibView.swift
//  TheWeatherApp
//
//  Created by Martin Pastorin on 08/01/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import UIKit

class XibView: UIView {
    
    // MARK: - Properties
    var xibContentView: UIView? {
        fatalError("Usage of XibView requires a xibContentView variable")
    }
    
    // MARK: - Initializer
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    // MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupUI()
    }
    
    // MARK: - Helpers
    private func initialize() {
        Bundle.main.loadNibNamed(String(describing: type(of: self)), owner: self, options: nil)
        
        guard let contentView = xibContentView else {
            return
        }
        
        setupUI()
        
        addSubview(contentView)
        contentView.frame = bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    
    // MARK: - Overridable
    func setupUI() {
        
    }
}
